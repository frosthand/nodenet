/*
 * test1_subscriber.cpp
 *
 *  Created on: Nov 26, 2012
 *      Author: vrobot
 *
 *      Simple subscriber example
 */


#include "test1.h"
#include <nodenet/subscriber.h>
#include <frosttools/threads.hpp>

using namespace nodenet;

int toReceive = 2000;
int received = 0;
/// Handler for MessageTest1 message type
void onMessage(const testmsg::MessageTest1 &msg, void * userdata)
{
	printf("Got message: %s\n", msg.message.c_str());
	received++;
}

int main(int argc, char * argv[])
{
	int time_sleep = 10;
	int time_update = 10;
	/// Create node "test_subscriber", using ip networking.
	/// By default it searches master at 127.0.0.1:10112

	NodeIP node("test_subscriber", 10112);

	if(argc > 1)
	{
		time_sleep = atoi(argv[1]);
	}

	if(argc > 2)
	{
		time_update = atoi(argv[2]);
	}
	/// Create subscriber for topic "test_msg"
	//SubscriberBase subscriber(&node,"test_msg");
	Subscriber<testmsg::MessageTest1> subscriber(&node, "test_msg", &onMessage, NULL);

	/// use function onMessage to handle appropriate message (MessageTest1)
	//subscriber.subscribe_fn(&onMessage);

	node.run();
	/// update node infinitely
	while(true)
	{
		//node.updateOnce(10);
		/// update internal network
		node.raiseEvents();
		/// give some CPU for other tasks
		Threading::sleep(time_sleep);

		if(toReceive > 0 && received >= toReceive)
		{
			printf("Received %d packets, exiting\n", received);
			break;
		}
	}
	node.stop();
	return 0;
}
