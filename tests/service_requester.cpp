/*
 * service_requester.cpp
 *
 *  Created on: Jul 17, 2013
 *      Author: vrobot
 */


#include "test1.h"
#include <nodenet/service.h>
#include <frosttools/threads.hpp>

// Generated message classes
#include "testmsg/RequestOp.hpp"
#include "testmsg/ResponseOp.hpp"

using namespace nodenet;

int toSend = 2000;

void onExit()
{
	printf("Shit the oops!\n");
}

bool calc(const testmsg::RequestOp & request, int & result)
{
	switch(request.op)
	{
	case '+':
		result = request.a + request.b;
		break;
	case '-':
		result = request.a - request.b;
		break;
	case '*':
		result = request.a * request.b;
		break;
	case '/':
		result = request.a / request.b;
		break;
	default:
		return false;
	}
	return true;
}

int main(int argc, char * argv[])
{
	atexit(&onExit);
	char address[128] = "ip:127.0.0.1:10112";
	/// Create node, using ip networking.
	/// By default it searches master at 127.0.0.1:10112

	NodeIP node("test_requester");

	// try to link to localhost:10112 with 100ms reconnect timeout
	node.link("ip:127.0.0.1:10112", 100);

	// create service client
	nodenet::ServiceClient<testmsg::RequestOp, testmsg::ResponseOp> client(&node, "testservice");

	node.run();

	int maxRequests = 10;
	int requestNum = 0;

	client.waitProvider();

	while(requestNum < maxRequests)
	{
		if(client.connections() > 0)
		{
			client.request.a = rand()/3 % 100;
			client.request.b = rand()/3 % 100;
			client.request.op = '+';

			ServiceResult result = client.send();
			if(result == ServiceDone)
			{
				int testResult = 0;
				calc(client.request, testResult);
				if(testResult == client.response.result)
					printf("Test: Request %d completed: %d %c %d = %d\n", requestNum, client.request.a, client.request.op, client.request.b, client.response.result);
				else
					printf("Test: Request %d completed with wrong result: %d %c %d = %d instead of %d\n", requestNum, client.request.a, client.request.b, client.request.op, client.response.result, testResult);
			}
			else
			{
				printf("Test: Failed to process request %d\n", requestNum);
			}
			requestNum++;
		}
		else
		{
			printf("Test: no providers found\n", requestNum);
			Threading::sleep(500);
		}
	}

	node.stop();
	return 0;
}

