/*
 * service_responder.cpp
 *
 *  Created on: Jul 17, 2013
 *      Author: Dmitry Kargin
 *      dv_frost@mail.ru
 */

#include "test1.h"
#include <nodenet/service.h>
#include <frosttools/threads.hpp>

// Generated message classes
#include "testmsg/RequestOp.hpp"
#include "testmsg/ResponseOp.hpp"

using namespace nodenet;
using namespace testmsg;

void onExit()
{
	printf("Shit the oops!\n");
}

int main(int argc, char * argv[])
{
	atexit(&onExit);
	/// Create node, using ip networking, binded to port 10112
	NodeIP node("test_requester", 10112);

	node.run();

	nodenet::ServiceProvider<RequestOp, ResponseOp> server(&node, "testService");

	while(true)
	{
		RequestOp request;
		if(server.wait() && server.getRequest(request))
		{
			ResponseOp response;
			switch(request.op)
			{
			case '+':
				response.result = request.a + request.b;
				break;
			case '-':
				response.result = request.a - request.b;
				break;
			case '*':
				response.result = request.a * request.b;
				break;
			case '/':
				response.result = request.a / request.b;
				break;
			}
			printf("Request %d %c %d = %d\n", request.a, request.op, request.b, response.result);
			server.pushResponse(response);
		}
	}

	node.stop();
	return 0;
}




