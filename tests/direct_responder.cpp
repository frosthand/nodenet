#include "rnsAPI.h"

void exampleClientPart()
{
}
// Example for robot driver

void exampleRobotPart()
{
	RNSNetInterface::Pointer net;
	RobotInterface::Pointer interface = RobotInterface::Connect("robotA", net);

	int updateTimeoutMS = 50;

	while(true)
	{
		// It is recommended to send odometry first, scan - second
		// sending odometry
		{
			RobotPose odometry;
			timestamp_t timestamp = net->GetLocalTime();
			// obtain odometry here
			interface->sendOdometry(odometry, net->GetLocalTime());
		}
		// sending scan
		{
			Scan scan;
			// obtain scan here ...
			interface->sendScan(scan, net->GetLocalTime());
		}

		net->waitForEvents(updateTimeoutMS);
	}
}
