/*
 * rnsAPI.h
 *
 *  Created on: Jun 28, 2013
 *      Author: vrobot
 */

#ifndef RNSAPI_H_
#define RNSAPI_H_

#include <nodenet/node.h>
#include <nodenet/transport_ip.h>
#include <frosttools/time.hpp>

#include <rnsmsg/Scan.hpp>
#include <rnsmsg/StampedPose.hpp>

class SimpleRefs
{
public:
	int refs;

	SimpleRefs() : refs(0) {}

	int addRef()
	{
		return ++refs;
	}
	int decRef()
	{
		return --refs;
	}
};
typedef unsigned long timestamp_t;
// does all the RNSNet networking
class RNSNetInterface: protected SimpleRefs
{
public:
	typedef nodenet::RefCountedPtr<RNSNetInterface> Pointer;
	friend class nodenet::RefCountedPtr<RNSNetInterface>;
	/// get obtain local timestamp
	timestamp_t getLocalTime() const;
	/// link to nodenet using single node address
	int link(const char * address);
	/// if connected to nodenet system
	bool connected() const;
	/// wait for connection to nodenet
	bool waitForConnection(int timeoutMS = 0);
	/// wait for incoming events
	bool waitForEvents(int timeoutMS = 0);

	nodenet::NodeIP * getNode()
	{
		return &node;
	}
protected:
	RNSNetInterface();
	nodenet::NodeIP node;
	/// run internal thread system
	int threadingRun();
	/// stop internal thread system
	int threadingStop();

	mutable Timer localTimer;
};

struct RobotPose
{
	float x,y, phi;
};

struct Scan
{
	std::vector<float> ranges;
	float minRange;
	float maxRange;
	float angleRes;
	float minAngle;
	float maxAngle;

	RobotPose sensorPose;
};

struct RobotData
{
	float size[3];				// Robot bounds, x, y, z, [m]
	float maxVelocity[2];		// Technical velocity limits
};

// Robot part. Sends new data, receives velocity commands
class RobotInterface : protected SimpleRefs
{
public:
	typedef nodenet::RefCountedPtr<RobotInterface> Pointer;
	friend class nodenet::RefCountedPtr<RobotInterface>;

	bool checkFeature(const char * feature);

	static Pointer Connect(const char * robotName, RNSNetInterface::Pointer ptr);
	// send scan to server
	void sendScan(const Scan & scan, timestamp_t timestamp);
	// send odometry/IMU accumulated odometry data to server
	void sendOdometry(const RobotPose & odometryPose, timestamp_t timestamp);
	// called when got new velocity command
	virtual void onVelocityCommand(float velocity[2]);
	// when any system requests
	virtual void onRobotGeometryRequest(float dimensions[2]);

	timestamp_t getLocalTime() const;
protected:
	RobotInterface(const char * robotName, RNSNetInterface::Pointer ptr);
	virtual ~RobotInterface();

	RNSNetInterface::Pointer interface;
	nodenet::Publisher<rnsmsg::Scan> pubScan;
	nodenet::Publisher<rnsmsg::StampedPose> pubOdom;
};

// Interface to send laser data to server
class MapperInterface
{
	RNSNetInterface::Pointer interface;
public:
	MapperInterface(RNSNetInterface::Pointer interface)
	:interface(interface)
	{
	}
	~MapperInterface();

	/// request robot position from server
	bool requestRobotPosition(RobotPose & pose);
	/// link to mapper server
	void link(const char * address, char * robotName);
	/// close connection
	void close();
	/// is connected to mapper
	bool connected() const;
};

struct MoveTask
{
	float pose[3];
	float velocity[3];
	enum Flags
	{
		FlagsAngle = 1,				// Use angle in task
		FlagsLinearVelocty = 2,		// Use linear velocity in pathfinding task
		FlagsAngularVelocity = 4,	// Use angular velocity in pathfinding task
	};
	int flags;						// Actual pathfinding flags
};

// Interface to pathfinding & pathFollowing system
class PilotInterface : protected SimpleRefs
{
public:
	typedef nodenet::RefCountedPtr<PilotInterface> Pointer;
	friend class nodenet::RefCountedPtr<PilotInterface>;
	// wait until next command arrives, i.e notification from pathinding server
	bool waitEvent(long timeout = 0);
	// issue order to move to specific point
	int moveToTarget(float mapX, float mapY);
	// issue order to move to point with angle
	int moveToTarget(float mapX, float mapY, float mapPhi);

	int addMoveTask(const MoveTask & task, bool override);
	// called when issued path is complete
	virtual void onPathComplete() = 0;
	// called when specific path waypoint is reached
	virtual void onWaypointReached(int index, const MoveTask & waypoint) = 0;
	// no path is found
	virtual void onPathError() = 0;
	// if path is canceled from somewhere
	virtual void onPathCanceled() = 0;

	/// link to mapper server
	void link(const char * address, char * robotName);
	/// close connection
	void close();
	/// is connected to mapper
	bool connected() const;
protected:
	virtual ~PilotInterface() {}
};


#endif /* RNSAPI_H_ */
