/*
 * test1.h
 *
 *  Created on: Nov 26, 2012
 *      Author: vrobot
 */

#ifndef TEST1_H_
#define TEST1_H_

#include <nodenet/node.h>
#include <nodenet/transport_ip.h>

#include <stdio.h>
#ifdef _MSC_VER
#define snprintf _snprintf
#endif

#include "testmsg/MessageTest1.hpp"

const int testPort = 10111;

#endif /* TEST1_H_ */
