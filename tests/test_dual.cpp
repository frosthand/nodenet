/*
 * test_dual.cpp
 *
 *  Created on: Nov 29, 2012
 *      Author: vrobot
 *
 *      Testing network latency and delay stabiliny
 *
 *  	test1: one publisher and one subscriber
 *  	test2: one publisher and four subscribers
 */


#include "test1.h"

#include <nodenet/subscriber.h>
#include <nodenet/publisher.h>

#include <time.h>

#include <frosttools/time.hpp>
#include <frosttools/threads.hpp>

#include <stdio.h>

using namespace nodenet;

const char * topicName = "test";
size_t maxMessages = 50;

int messagesSent = 0;

bool done = false;

int send_delay = 10;	/// sending delay in ms
int read_delay = 1;	/// reading delay in ms

int testSubscribersCount = 0;
Timer publishTimer;
Timer subscribeTimer;

void thread_publish(const char * name)
{	
	NodeIP node(name, testPort);
	Timer timer;

	Publisher<testmsg::MessageTest1> publisher(&node, topicName);

	testmsg::MessageTest1 msg;

	char tmpBuffer[255];

	long prevSend = timer.currentTimeMS();
	node.run();
	/// while anything is connected
	while(/*node.alive() > 0 &&*/ !done)
	{
		/// update tcp network
		if(publisher.connections() == testSubscribersCount)
		{
			/// update message and send it to everyone
			snprintf(tmpBuffer, sizeof(tmpBuffer), "Hello from test:%d", messagesSent);

			msg.timestamp = timer.currentTimeMS();
			printf("%s sending message#%d at %d, delta=%dns\n", name, messagesSent, (int)msg.timestamp, (int)(msg.timestamp - prevSend));
			prevSend = msg.timestamp;
			msg.message = tmpBuffer;
			publisher.publish(msg);
			messagesSent++;
		}
		Threading::sleep(send_delay);
	}
	printf("%s sending data complete, sent %d messages\n", name, messagesSent);
	node.stop();
}

class Receiver
{
public:
	const char * name;
	int messagesRecieved;
	Timer timer;
	Receiver(const char * n)
	{
		name = n;
		recievedTimestamp = timer.currentTimeMS();
		messagesRecieved = 0;
	}
	uint32_t recievedTimestamp;

	static void s_onMessage(const testmsg::MessageTest1 &msg, void * userdata)
	{
		((Receiver*)userdata)->onMessage(msg);
	}

	void onMessage(const testmsg::MessageTest1 &msg)
	{
		uint32_t currentTimestamp = timer.currentTimeMS();
		int delta = currentTimestamp - recievedTimestamp;
		int latency = (currentTimestamp - msg.timestamp);
		printf("%s got message: %s, delta=%dns, latency=%dns, send=%d, recv=%d\n", name, msg.message.c_str(), delta, latency, msg.timestamp, currentTimestamp);
		messagesRecieved++;
		if(messagesRecieved == maxMessages)
			done = true;
		recievedTimestamp = currentTimestamp;
	}
};

void thread_listen(const char * name)
{	
	/// create node with specified name
	NodeIP node(name);
	Receiver receiver(name);

	Subscriber<testmsg::MessageTest1> subscriber(&node, topicName, &Receiver::s_onMessage, &receiver);

	node.link("ip:127.0.0.1:10111", 500);
	node.run();
	/// while anything is connected
	while(/*node.alive() && */!done)
	{
		/// update tcp network
		//node.update(read_delay);
		node.raiseEvents();
		Threading::sleep(send_delay);
	}
}

void run_test1()
{
	printf("===============Test 1================\n");
	printf("One publisher one subscriber\n");

	Threading::thread threads[2];

	//pthread_t id[2];

	messagesSent = 0;
	testSubscribersCount = 1;

	
	threads[0].run(thread_publish, "publisher1");
	threads[1].run(thread_listen, "subscriber1");

	void * result = NULL;
	threads[0].join();
	threads[1].join();
	done = false;
}

void run_test2()
{
	printf("===============Test 2================\n");
	printf("One publisher multiple listeners\n");
	Threading::thread threads[5];

	messagesSent = 0;
	testSubscribersCount = 4;

	threads[0].run(thread_publish, "publisher1");
	threads[1].run(thread_listen, "subscriber1");
	threads[2].run(thread_listen, "subscriber2");
	threads[3].run(thread_listen, "subscriber3");
	threads[4].run(thread_listen, "subscriber4");
	
	void * result = NULL;

	threads[0].join();
	threads[1].join();
	threads[2].join();
	threads[3].join();
	threads[4].join();

	done = false;
}

int main(int argc, char * argv[])
{
	//timer.start();
	if(argc == 3)
	{
		send_delay = atoi(argv[1]);
		read_delay = atoi(argv[2]);
	}
	run_test1();
	run_test2();

	printf("All the tests are complete\n");
	return 0;
}
