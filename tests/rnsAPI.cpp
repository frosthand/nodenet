/*
 * rns.cpp
 *
 *  Created on: Jun 28, 2013
 *      Author: vrobot
 */

#include "rnsAPI.h"

timestamp_t RNSNetInterface::getLocalTime() const
{
	return localTimer.currentTimeMS();
}

int RNSNetInterface::link(const char* address)
{
	return node.link(address, 500);
}

bool RNSNetInterface::connected() const
{
	return node.alive();
}

bool RNSNetInterface::waitForConnection(int timeoutMS)
{
	return true;
}

bool RNSNetInterface::waitForEvents(int timeoutMS)
{
	if(node.raiseEvents(timeoutMS))
		return true;
	return false;
}

int RNSNetInterface::threadingRun()
{
	node.run();
	return 0;
}

int RNSNetInterface::threadingStop()
{
	node.stop();
	return 0;
}

bool RobotInterface::checkFeature(const char* feature)
{
	return false;
}

RobotInterface::Pointer RobotInterface::Connect(const char* robotName,
		RNSNetInterface::Pointer ptr)
{
	RobotInterface::Pointer interface(new RobotInterface(robotName, ptr));
	return interface;
}

timestamp_t RobotInterface::getLocalTime() const
{
	if(interface != NULL)
		return interface->getLocalTime();
	return 0;
}

void RobotInterface::sendScan(const Scan& scan, timestamp_t timestamp)
{
	rnsmsg::Scan message;
	message.timestamp = getLocalTime();
	// TODO: Fill in the message
	pubScan.publish(message, 0);
}

void RobotInterface::sendOdometry(const RobotPose& odometryPose,
		timestamp_t timestamp)
{
	rnsmsg::StampedPose message;
	message.timestamp = getLocalTime();
	message.pose.x = odometryPose.x;
	message.pose.y = odometryPose.x;
	message.pose.phi = odometryPose.phi;
	// TODO: Fill in the message
	pubOdom.publish(message, 0);
}

void RobotInterface::onVelocityCommand(float velocity[2])
{
}

void RobotInterface::onRobotGeometryRequest(float dimensions[2])
{
}

RobotInterface::RobotInterface(const char * robotName, RNSNetInterface::Pointer ptr)
:pubScan(ptr->getNode(), (std::string(robotName)+"/scan").c_str())
,pubOdom(ptr->getNode(), (std::string(robotName)+"/odom").c_str())
{
	interface = ptr;
}

RobotInterface::~RobotInterface()
{
}

MapperInterface::~MapperInterface()
{
}

bool MapperInterface::requestRobotPosition(RobotPose& pose)
{
	return false;
}

void MapperInterface::link(const char* address, char* robotName)
{
}

void MapperInterface::close()
{
}

bool MapperInterface::connected() const
{
	return false;
}

bool PilotInterface::waitEvent(long timeout)
{
}

int PilotInterface::moveToTarget(float mapX, float mapY)
{
}

int PilotInterface::moveToTarget(float mapX, float mapY, float mapPhi)
{
}

int PilotInterface::addMoveTask(const MoveTask& task, bool override)
{
}

void PilotInterface::link(const char* address, char* robotName)
{
}

void PilotInterface::close()
{
}

bool PilotInterface::connected() const
{
}
