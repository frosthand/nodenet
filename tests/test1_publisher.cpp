/*
 * test1_publisher.cpp
 *
 *  Created on: Nov 26, 2012
 *      Author: vrobot
 *
 *      Publisher example
 */

#include "test1.h"
#include <nodenet/publisher.h>
#include <frosttools/threads.hpp>

using namespace nodenet;

int toSend = 2000;

int main(int argc, char * argv[])
{
	/// Create node, using ip networking.
	/// By default it searches master at 127.0.0.1:10112

	NodeIP node("test_publisher");

	/// Create publisher for topic "test_msg"
	Publisher<testmsg::MessageTest1> publisher(&node, "test_msg");

	testmsg::MessageTest1 msg;
	int counter = 0;

	// try to link to localhost:10112 with 100ms reconnect timeout
	node.link("ip:127.0.0.1:10112", 100);

	//node.run();

	char buffer[255];
	while(true)
	{
		/// fill message data
		snprintf(buffer, sizeof(buffer), "Hello from test, %d", counter);
		msg.message = buffer;
		if(publisher.connections() > 0)
		{
			/// send message itself
			publisher.publish(msg);
			counter++;
		}
		if(counter >= toSend)
		{
			printf("Sent %d messages - exiting\n", counter);
			break;
		}
		//node.update(10);
		node.updateOnce(10);
		Threading::sleep(50);
	}

	//node.stop();
	return 0;
}
