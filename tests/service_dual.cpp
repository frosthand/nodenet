/*
 * service_dual.cpp
 *
 *  Created on: Jul 17, 2013
 *      Author: vrobot
 */



/*
 * service_requester.cpp
 *
 *  Created on: Jul 17, 2013
 *      Author: vrobot
 */


#include "test1.h"
#include <nodenet/service.h>
#include <frosttools/threads.hpp>

using namespace nodenet;

int toSend = 2000;

int main(int argc, char * argv[])
{
	char address[128] = "ip:127.0.0.1:10112";
	/// Create node, using ip networking.
	/// By default it searches master at 127.0.0.1:10112

	NodeIP node("test_requester");
	// try to link to localhost:10112 with 100ms reconnect timeout
	node.link("ip:127.0.0.1:10112", 100);

	node.run();

	while(true)
	{

		//node.update(10);
		node.updateOnce(10);
		Threading::sleep(50);
	}

	node.stop();
	return 0;
}


