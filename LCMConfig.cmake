INCLUDE(FindPackageHandleStandardArgs)

SET(LCM_IncludeSearchPaths
  /usr/include/
  /usr/local/include/
  /opt/local/include
  ${CMAKE_CURRENT_SOURCE_DIR}/include
)

SET(LCM_LibrarySearchPaths
  /usr/lib/
  /usr/local/lib/
  /opt/local/lib/
)

FIND_PATH(LCM_INCLUDE_DIR lcm/lcm_coretypes.h
  PATHS ${LCM_IncludeSearchPaths}
)

FIND_LIBRARY(LCM_LIBRARY_OPTIMIZED
  NAMES lcm
  PATHS ${LCM_LibrarySearchPaths}
)

# Handle the REQUIRED argument and set the <UPPERCASED_NAME>_FOUND variable
FIND_PACKAGE_HANDLE_STANDARD_ARGS(LCM "Could NOT find LCM library (LCM)"
  LCM_LIBRARY_OPTIMIZED
  LCM_INCLUDE_DIR
)

# Collect optimized and debug libraries
#HANDLE_LIBRARY_TYPES(LCM)

MARK_AS_ADVANCED(
  LCM_INCLUDE_DIR
  LCM_LIBRARY_OPTIMIZED
)

FIND_PROGRAM(LCM_GEN_EXECUTABLE lcm-gen PATHS tools /usr/bin /usr/local/bin)
set(LCM_GEN_FOUND FALSE)

IF (LCM_GEN_EXECUTABLE)
  set(LCM_GEN_FOUND TRUE)
  MESSAGE(STATUS "lcm-gen found at ${LCM_GEN_EXECUTABLE}")
  # Define the WRAP_PROTO function
    
  # TargetName - target to generate files for
  # GenDirectory - directory to place generated headers
  # ARGN - list with .lcm files
  FUNCTION(LCM_MESSAGE_GEN2 TargetName GenDirectory)
  
  	MESSAGE(STATUS "lcm-gen generating message for ${TargetName} for dir ${GenDirectory}")
  	
  	IF (NOT TargetName)
  		MESSAGE(SEND_ERROR "Error: LCM message generator called without specified output list")
  		MESSAGE(SEND_ERROR "Usage: LCM_MESSAGE_GEN2(TargetName GenDirectory ...)")
    	RETURN()
  	ENDIF(NOT TargetName)
  	
  	IF (NOT GenDirectory)
  		MESSAGE(SEND_ERROR "Error: LCM message generator called without specified directory")
  		MESSAGE(SEND_ERROR "Usage: LCM_MESSAGE_GEN2(TargetName GenDirectory ...)")
    	RETURN()
  	ENDIF(NOT GenDirectory)
  	
    IF (NOT ARGN)
    	MESSAGE(SEND_ERROR "Error: LCM message generator called without any proto files")
    	MESSAGE(SEND_ERROR "Usage: LCM_MESSAGE_GEN2(TargetName GenDirectory ...)")
    	RETURN()
    ENDIF(NOT ARGN)

    SET(INCL)
    
    SET(Generated)  
    
    include_directories(${GenDirectory})
    
    FOREACH(FIL ${ARGN})
      GET_FILENAME_COMPONENT(ABS_FIL ${FIL} ABSOLUTE)
      GET_FILENAME_COMPONENT(FIL_WE ${FIL} NAME_WE)
      #LIST(APPEND ${VAR} "${CMAKE_CURRENT_BINARY_DIR}/${FIL_WE}.lcm.cc")
      SET(INCL_ITEM "${FIL_WE}.lcm.h")
      LIST(APPEND Generated ${INCL_ITEM})
      #MESSAGE(STATUS "invoking lcm-gen for ${FIL}:${FIL_WE}:${ABS_FIL}")
      add_custom_target(
      		#OUTPUT ${INCL_ITEM}
      		#TARGET ${TargetName}
      		${TargetName}-gen ALL
		    COMMAND  ${LCM_GEN_EXECUTABLE} -x --cpp-hpath ${GenDirectory} ${FIL}
			WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
		    #SOURCES ${ABS_FIL}
		    DEPENDS ${FIL}		    
		    COMMENT "Running protocol buffer compiler on ${FIL}"
		    VERBATIM
        )
        #set_proterty(TARGET ${TargetName} APPEND PROPERTY DEPENDS ${FIL})
        add_dependencies(${TargetName} ${TargetName}-gen)
    ENDFOREACH(FIL)
    #MESSAGE(STATUS "generated ${${GenFiles}} at \"${GenDirectory}\"")
    
  ENDFUNCTION(LCM_MESSAGE_GEN2)
ELSE(LCM_GEN_EXECUTABLE)
MESSAGE(SEND_ERROR "Error: lcm-gen not found")
ENDIF(LCM_GEN_EXECUTABLE)
