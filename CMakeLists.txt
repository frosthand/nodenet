# main project - Robot
cmake_minimum_required(VERSION 2.6)

project(nodenet)

if(WIN32)
	add_definitions( -D_CRT_SECURE_NO_WARNINGS )
endif(WIN32)

#set the default path for built executables to the "bin" directory
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
#set the default path for built libraries to the "lib" directory
set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/lib)

#set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${PROJECT_SOURCE_DIR})

include(LCMConfig.cmake)

include(doxyConfig.cmake)

set(DOXY_OUTPUT_LANGUAGE "English")
set(DOXY_INPUT ${PROJECT_SOURCE_DIR})
ADD_DOCUMENTATION(doc doc/Doxyfile)

include_directories(include)
include_directories( ${FROSTTOOLS_INCLUDE_DIR} )

include(CPack)

add_subdirectory(server)

option(BUILD_NODENET_TESTS "Compile nodenet tests" OFF)
if(BUILD_NODENET_TESTS)
	add_subdirectory(tests)
endif(BUILD_NODENET_TESTS)

# install target
install(DIRECTORY include/nodenet DESTINATION include FILES_MATCHING PATTERN "*.h")
install(DIRECTORY include/nodenet DESTINATION include FILES_MATCHING PATTERN "*.hpp")

#install(TARGETS nodenet nodenet-ip DESTINATION lib)

# uninstall target
configure_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/cmake_uninstall.cmake.in"
    "${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake"
    IMMEDIATE @ONLY)

add_custom_target(uninstall
    COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake)
