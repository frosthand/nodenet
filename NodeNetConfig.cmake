# Find and configure nodenet
# Defines following variables:
# NODENET_FOUND
# NODENET_INCLUDE_DIR
# NODENET_LIBRARIES
# NODENET_DEFINITIONS

find_package(FrostTools REQUIRED)

if(FrostTools_FOUND)
	include_directories(${FROSTTOOLS_INCLUDE_DIR})
endif(FrostTools_FOUND)

find_path(NODENET_INCLUDE_DIR nodenet/node.h HINTS /usr/include /usr/local/include ${NodeNet_DIR}/include)
find_library(NODENET_LIBRARY_IP NAMES nodenet-ip HINTS /usr/lib /usr/local/lib ${NodeNet_DIR}/lib)

#set(NODENET_LIBRARIES ${NODENET_LIBRARY})
set(NODENET_FOUND TRUE)

if(NODENET_INCLUDE_DIR)
	message(STATUS "NodeNet include found : ${NODENET_INCLUDE_DIR}")	
	set(${NODENET_INCLUDE_DIRS} ${NODENET_INCLUDE_DIR} ${FROSTTOOLS_INCLUDE_DIR})
else(NODENET_INCLUDE_DIR)
	message(STATUS "NodeNet include not found")
	set(NODENET_FOUND FALSE)
endif(NODENET_INCLUDE_DIR)

if(NODENET_LIBRARY_IP)
	set(NODENET_LIBRARIES ${NODENET_LIBRARY_IP})
	message(STATUS "NodeNet-ip lib found : ${NODENET_LIBRARY_IP}")	
else(NODENET_LIBRARY_IP)
	message(STATUS "NodeNet library not found")
	set(NODENET_FOUND FALSE)
endif(NODENET_LIBRARY_IP)
