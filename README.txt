Node network for syborobotics navigation system. Its architecture was inspired mostly by ROS.

Key ideas:
- content-specific connection
- support custom transports: udp, tcp, com-com
- robust packet sending/receiving

Requirements:
	frosttools
	LCM for code generation

Consists of plugin-based server application and API library to embed in other projects

/include/nodenet
/lib
	Compiled libraries
/tests
	Tests and examples for library
Building

	$mkdir build
	$cd build
	$cmake ..
	$make
	#make install
Built-in message types

	int8_t		8-bit signed integer
	int16_t		16-bit signed integer
	int32_t		32-bit signed integer
	int64_t		64-bit signed integer
	float		32-bit IEEE floating point value
	double		64-bit IEEE floating point value
	string		UTF-8 string
	boolean		true/false logical value
	byte		8-bit value 

Running
