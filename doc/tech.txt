wait variants:

node.wait() - wait for any topic event (any client)
node.raiseEvents() - raise callbacks for any client right in this thread

client.wait() - wait for client event
client.raiseEvents() - raise callbacks for specific client