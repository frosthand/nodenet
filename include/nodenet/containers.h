/*
 * containers.h
 *
 *  Created on: Jul 31, 2013
 *      Author: vrobot
 */

#ifndef CONTAINERS_H_
#define CONTAINERS_H_

//! Implementation for some simple containers used in nodenet


namespace nodenet
{
	//! Double linked list
	/*! Totally thread-unsafe
	 *	Contained node should have direct (not smartptr) 'next' and 'prev' pointers.
	 */
	template<class T> struct List
	{
		T * head;
		T * tail;
		int N;

		List()
		{
			head = NULL;
			tail = NULL;
			N = 0;
		}

		size_t size() const
		{
			return N;
		}

		void add(T * node)
		{
			if(tail == NULL)
			{
				head = node;
				tail = node;
			}
			else
			{
				tail->next = node;
				node->prev = tail;
			}
			N++;
		}

		void erase(T * node)
		{
			if(head == node)
				head = node->next;
			if(tail == node)
				tail = node->prev;
			if(node->prev != NULL)
				node->prev->next = node->next;
			if(node->next != NULL)
				node->next->prev = node->prev;
			N--;
		}

		/// move first to last
		void rotate()
		{
			if(head == tail)
				return;
			T * tmp = head;
			erase(head);
			add(head);
		}
	};

	template<class Target>
	class ListPart
	{
	public:
		ListPart()
		{
			prev = NULL;
			next = NULL;
		}
		virtual ~ListPart()
		{
		}
	protected:
		Target * prev;
		Target * next;

		friend class List<Target>;
	};
}
#endif /* CONTAINERS_H_ */
