/*
 * topic.h
 *
 *  Created on: Feb 1, 2013
 *      Author: vrobot
 */
#pragma once

#include <string>
#include <nodenet/entity.h>

namespace nodenet
{
	class Node;
	class Topic;

	class SubscriberBase;
	class PublisherBase;

	//! Holds information about local topic.
	/*! Keeps pointers to local subscribers and publishers
	 	Every sub/pub should deatach from topic before being destroyed
	 */
	class Topic : public Entity
	{
		friend class Node;
		friend class SubscriberBase;
		friend class PublisherBase;
	public:
		void publishMessage(const BaseMessageLCM & msg, SendFlags_t flags);
		/// get remote references count ( subscribers + publishers )
		size_t getRemoteSubs() const;
		size_t getRemotePubs() const;

		int getSources() const
		{
			return publishers.size();
		}

		int getSinks() const
		{
			return subscribers.size();
		}

		EntityType getType() const
		{
			return EntityTopic;
		}

		int raiseEvents();
	protected:
		size_t accessKey;		//!< key used to send packets

		typedef List<SubscriberBase> Subscribers;
		Subscribers subscribers;						//!< container for sinks

		typedef List<PublisherBase> Publishers;
		Publishers publishers;							//!< container for sources

		Topic();
		~Topic();
		//
		void detachAllClients();

		/// notify all local subscribers about new message
		int pushMessage(int type, const DataBufferPtr & buffer);
		friend class RefCountedPtr<Topic>;
	};

	typedef RefCountedPtr<Topic> TopicPtr;
} // namespace nodenet

