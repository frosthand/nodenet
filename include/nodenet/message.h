/*
 * message.h
 *
 *  Created on: May 15, 2013
 *      Author: vrobot
 */

#ifndef _RNS_MESSAGE_H_
#define _RNS_MESSAGE_H_

#include <stdint.h>
#include <string>

/*
 * Wrapper tools for LCM-generated messages
 */

struct MessageTypeInfo
{
	int64_t hash;
	std::string name;

	MessageTypeInfo()
	:hash(0) {}

	MessageTypeInfo(const MessageTypeInfo & info) : hash(info.hash), name(info.name) {}

	bool isEmpty() const
	{
		return hash == 0 && name == "";
	}
};

inline bool operator== (const MessageTypeInfo & a, const MessageTypeInfo & b)
{
	return a.name == b.name && a.hash == b.hash;
}

typedef int64_t hash_t;

// Base class for any LCM message wrapper

class BaseMessageLCM
{
public:
	virtual ~BaseMessageLCM() {}
	virtual int encode(void *buf, int offset, int maxlen) const = 0;
	virtual int getEncodedSize() const = 0;
	virtual int decode(const void *buf, int offset, int maxlen) = 0;
	virtual int64_t getHash() const = 0;

	virtual const char* getTypeName() const = 0;

	template<class Impl> static void fillTypeInfo(MessageTypeInfo * info);
};

template<class Impl> class MessageLCM : public BaseMessageLCM
{
public:
	Impl * pImpl;

	MessageLCM(Impl * impl = NULL) : pImpl(impl) {}

	int encode(void *buf, int offset, int maxlen) const
	{
		return pImpl->encode(buf, offset, maxlen);
	}

	int getEncodedSize() const
	{
		return pImpl->getEncodedSize();
	}

	int decode(const void *buf, int offset, int maxlen)
	{
		return pImpl->decode(buf, offset, maxlen);
	}

	int64_t getHash() const
	{
		return pImpl->getHash();
	}

	const char* getTypeName() const
	{
		return pImpl->getTypeName();
	}
};

template<class Impl> inline void BaseMessageLCM::fillTypeInfo(MessageTypeInfo * info)
{
	if(info != NULL)
	{
		info->name = Impl::getTypeName();
		info->hash = Impl::getHash();
	}
}

template<class Impl> inline MessageTypeInfo GetTypeInfo()
{
	MessageTypeInfo result;
	result.name = Impl::getTypeName();
	result.hash = Impl::getHash();
	return result;
}

#endif /* MESSAGE_H_ */
