/*
 * publisher.h
 *
 *  Created on: Nov 26, 2012
 *      Author: Dmitriy Kargin
 */

#pragma once

#include "node.h"
#include "message.h"

namespace nodenet
{
	class PublisherBase : public EntityClient
	{
		friend class Node;
	public:
		PublisherBase(const MessageTypeInfo & type);
		~PublisherBase();

		void attach(Node * node, const char * name);

		void detach();

		EntityPtr getEntity() const;

		int connections() const;

		size_t getSubscriptors() const;

		int raiseEvents()
		{
			return 0;
		}
	protected:

		PublisherBase * prev;
		PublisherBase * next;

		MessageTypeInfo typeMsg;

		friend class List<PublisherBase>;

		TopicPtr topic;

		int eventsCount() const
		{
			return 0;
		}

		void fillTypeInfo(MessageTypeInfo *  info);
		//void send(const MessageBase & msg, SendFlags_t flags = 0);

	};

	template<class Message> class Publisher : public PublisherBase
	{
		Message message;

	public:
		Publisher(Node * node, const char * name)
			:PublisherBase(GetTypeInfo<Message>())
		{
			attach(node, name);
		}

		void publish(const Message & msg, SendFlags_t flags = 0)
		{
			if(!valid())
				return;
			topic->publishMessage(MessageLCM<Message>((Message*)&msg), flags);
		}
	};
} // namespace nodenet
