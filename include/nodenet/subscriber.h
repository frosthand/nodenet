#pragma once

#include <mutex>
#include "message.h"
#include "node.h"

namespace nodenet
{
	//! Base class for any topic subscriber.
	/// Not supposed to be used directly
	class SubscriberBase : public EntityClient
	{
		friend class Node;
		friend class Topic;
		Node * node;
		TopicPtr topic;

		MessageTypeInfo typeMsg;
	public:
		SubscriberBase(const MessageTypeInfo & type);
		~SubscriberBase();

		//! Attach to network node
		void attach(Node * node, const char * topicName);
		//! Detach from network node
		void detach();

		EntityPtr getEntity() const;
	protected:
		SubscriberBase * prev;
		SubscriberBase * next;

		friend class List<SubscriberBase>;

		virtual int pushMessage(int type, const void * data, size_t length) = 0;
		//virtual int raiseEvents() = 0;
		//! get type description to check type compatibility
		virtual void fillTypeInfo(MessageTypeInfo *  info);
	};

	//! Subscriber for specific message type.
	/// Message class should be generated from message description
	template<class Message> class Subscriber : public SubscriberBase
	{
		friend class Node;

	protected:
		std::list<Message*> messages;	/// list of received messages

		/// Callback handler
		void (*fn_handler)(const Message & msg, void * userdata);
		void* userdata;			/// userdata for callback handler

		/// used to check new events
		int eventsCount() const
		{
			return messages.size();
		}
		/// called from node/topic when it got new message
		int pushMessage(int type, const void * data, size_t length)
		{
			Message * msg = new Message();
			if(msg->decode((void*)data, 0, length) < 0)
			{
				printf("Subscriber::pushMessage - failed to decode data\n");
				return -1;
			}
			//printf("pushMessage - adding to queue\n");
			guard.lock();
			messages.push_back(msg);
			guard.unlock();
			newData.notify_all();
			//printf("pushMessage - notified\n");
			return 0;
		}
		/// Invoking all attached message handlers
		int raiseEvents()
		{
			// 1. Get a copy of current message queue
			std::list<Message*> cached;
			{
				std::unique_lock<std::mutex> lock(guard);
				cached = std::move(messages);
			}

			int counter = 0;
			while(!cached.empty())
			{
				Message * msg = cached.front();
				cached.pop_front();
				if(fn_handler)
					fn_handler(*msg, userdata);
				counter++;
				delete msg;
			}
			return counter;
		}

	public:
		Subscriber(Node * node, const char * topic, void (*fn_handler)(const Message & msg, void * userdata), void * userdata)
			:SubscriberBase(GetTypeInfo<Message>()), fn_handler(fn_handler), userdata(userdata)
		{
			attach(node, topic);
		}
	};
} // namespace nodenet
