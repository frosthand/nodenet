#pragma once

#include <string.h>	// for memcpy
#if defined(WIN32) || defined(__WIN32__)
#include <ws2tcpip.h>
#else
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

#include <exception>

#include <nodenet/socket_ip.h>
#include <poll.h>

namespace nodenet
{
namespace ip
{
	class Peer
	{
	protected:
		bool dying;
		bool lockedSocketsArray;
		void writeLog(int level, const char * format, ...) const;

	public:
		virtual void lock() {}
		virtual void unlock() {}
		typedef size_t SlotHandle;
		static bool validSlot(SlotHandle handle)
		{
			return handle != (~(size_t)0);
		}
		/// Callback for every network event
		/// You should use it at least to safely manage slots
		class Callback
		{
		public:
			friend class Peer;
			virtual ~Callback(){}
			/// return true to accept TCP connection
			virtual bool onIncomingConnection(Peer * peer,  size_t listener, size_t listenerId) = 0;
			/// TCP connection was closed
			virtual void onSlotClosed(Peer * peer, size_t clientId) = 0;
			/// connected by TCP to remote host
			virtual void onSlotConnected(Peer * connection, size_t clientId) = 0;
			/// got data from any protocol
			virtual unsigned int onRecieve(Peer * connection, size_t clientId, const void * data, unsigned int size, PeerAddress * address) = 0;
			/// notify when slot should be released (to free attached userdata)
			virtual void onSlotReleased(Peer * connection, size_t clientId) = 0;
		};

		Callback * listener;

		Peer(int bufferSize);
		virtual ~Peer();
		// try to send, sync call
		virtual int send(SlotHandle peerId, const void * data, size_t size, PeerAddress * address = NULL);

		//virtual int sendBlocking(SlotHandle peerId, const void * data, size_t size, PeerAddress * address = NULL);
		// TODO: implement
		//virtual int recv(SlotHandle peerId, void * buffer, size_t maxLength);	/// read data from slot manually
		// TODO: implement

		int close(SlotHandle peerId);

		/// close all slots
		virtual void closeAll();

		typedef bool Result;

		SlotHandle listen( int port, void * userdata = NULL );
		SlotHandle connect( const PeerAddress * address, bool wait = false, void * userdata = NULL );
		SlotHandle createUDP( int port, void * userdata = NULL );
		void disconnect( SlotHandle slotId);
		size_t getClientsUsed() const;
		/// enable blocking/unblocking socket handling

		//void updateSlot(size_t slotId);	/// update slot manually

		//void setSlotMode(size_t slotId, SlotUpdateMode mode);
		void update(timeval &timeout);

		void setNoDelay(SlotHandle slotId, bool value);

		Socket::SlotState getSlotState(SlotHandle id) const;
		PeerAddress getLocalAddress(SlotHandle id) const;
		PeerAddress getRemoteAddress(SlotHandle id) const;
		void setSlotUserdata(SlotHandle id, void * userdata);
		void * getSlotUserdata(SlotHandle handle) const;
		//PeerAddress getSlotTargetAddress(SlotHandle id) const;
	protected:
		Socket & getSlot(SlotHandle handle);
		const Socket & getSlot(SlotHandle handle) const;
		SlotHandle makeHandle(size_t slotId) const;
		/// checks if slot handle is valid
		int checkSlotHandle(SlotHandle id) const;

		void checkSlots_poll(timeval &timeout);
		void checkSlots_select(timeval &timeout);

		void updateSlotState(size_t slotId);
		enum { maxClients = 32 };

		Socket * sockets;
#ifdef FROSTTOOLS_USE_POLL
		pollfd * polls;
#else
		fd_set readfds, writefds, exceptfds;
#endif
		size_t socketsAllocated;
		size_t socketsUsed;

		//int16_t getSlotEvents(size_t slotId) const;
		void callError(NetErrorType type) const;
	private:
		/// insert new socket
		size_t addSocket( const Socket & socket);
		/// check socket for recieving/connecting/accepting
		bool processTCPAccepting(size_t id);
		void processTCPConnecting(size_t id);
		void processReceiving(size_t id);
		//void processUDPRecieving(size_t id);
	};
} // namespace ip
} // namespace nodenet
