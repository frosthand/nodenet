namespace nodenet
{
	namespace ip
	{
		enum NetErrorType
		{
			Unknown,
			WouldBlock,
			SystemInterrupted,
			SocketCreation,
			ConnectionClosed,
			InvalidSlot,
			InvalidArgument,
			Memory,
			SlotsArrayLocked,
			BufferOverrun,
		};

		const char * getErrorString(NetErrorType type);

		/// Wrapper for network address
		struct PeerAddress
		{
			sockaddr_in from;
			int type;

			PeerAddress();

			PeerAddress(const PeerAddress &pa);

			PeerAddress & operator=(const PeerAddress & pa);

			void setHostname(const char * name);

			void setPort(int port);

			const char * getHostname() const;

			int getPort() const;
		};

		int mkaddr(void *addr, int *addrlen, const char *str_addr, const char *protocol);
	}
}
