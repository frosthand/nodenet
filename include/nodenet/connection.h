/*
 * connection.h
 *
 *  Created on: Feb 1, 2013
 *      Author: vrobot
 */

#pragma once

#include <stdint.h>
#include <stdlib.h>

#include <string>
#include <vector>
#include <list>
#include <mutex>

#include <nodenet/message.h>
#include <nodenet/bufferptr.h>

//#include <frosttools/lockfree.hpp>
//#include <frosttools/threads.hpp>

namespace nodenet
{
	enum SendFlags
	{
		SendFlagImmediate = 1,
		SendFlagUDP = 2,
		SendFlagCanDrop = 4,		/// drop this message if network is full
	};

	class Transport;
	class Topic;

	typedef uint32_t NodeKey;

	typedef std::string EndPoint;


	class DataTicket
	{
	public:
		enum State
		{
			StateEmpty,
			StateHeader,
			StateData
		}transmitState;
		int index;				/// request index
		DataBufferPtr buffer;

		uint16_t targetSlot;
		uint16_t requestId;

		size_t dataProcessed;

		size_t blockSize;
		std::vector<char> blockState;

		DataTicket * next;

		DataTicket()
		{
			transmitState = StateEmpty;
			targetSlot = 0;
			requestId = 0;
			blockSize = 0;
			dataProcessed = 0;
			index = 0;
			next = NULL;
		}

		DataTicket(DataBufferPtr ptr, int slot, int id)
		{
			transmitState = StateHeader;
			targetSlot = slot;
			requestId = id;
			blockSize = 0;
			dataProcessed = 0;
			index = 0;
			buffer = ptr;
			next = NULL;
		}

		DataTicket(const DataTicket &ticket)
		{
			*this = ticket;
		}

		DataTicket & operator=(const DataTicket &ticket)
		{
			buffer = ticket.buffer;
			dataProcessed = ticket.dataProcessed;
			blockSize = ticket.blockSize;
			index = ticket.index;
			transmitState = ticket.transmitState;
			targetSlot = ticket.targetSlot;
			requestId = ticket.requestId;
			return *this;
		}

		size_t remaining() const
		{
			return buffer.size() - dataProcessed;
		}

		void reset()
		{
			buffer = DataBufferPtr();
			transmitState = StateEmpty;
			dataProcessed = 0;
			targetSlot = 0;
			requestId = 0;
		}
	};

	// Storage for DataTickets, using pthreads mutex
	class TicketStorage
	{
		std::mutex guardSend;
		std::list<DataTicket> sendQueue;
	public:
		virtual ~TicketStorage(){}

		void clear()
		{
			std::unique_lock<std::mutex> lock(guardSend);
			sendQueue.clear();
		}
		// create new ticket and push it back
		virtual void pushTicket(DataTicket & ticket)
		{
			std::unique_lock<std::mutex> lock(guardSend);
			sendQueue.push_back(ticket);
		}

		virtual bool popTicket(DataTicket & ticket)
		{
			std::unique_lock<std::mutex> lock(guardSend);
			if(sendQueue.empty())
				return false;
			ticket = sendQueue.front();
			sendQueue.pop_front();
			return true;
		}
	};

	class Connection
	{
	protected:
		NodeKey remoteNode;			/// node, connection related to
		mutable int numRefs;

		TicketStorage sendQueueSystem;	// storage for system data. This data is sent first
		TicketStorage sendQueueUser;	// storage for user data.
	public:

		class Listener
		{
		public:
			virtual void onConnected() = 0;
			virtual void onClosed() = 0;
		};

		Listener * listener;

		void notifyConnectionCreated(){}

		int addRef() const
		{
			return ++numRefs;
		}

		int decRef() const
		{
			return --numRefs;
		}

		// slot 		- receiver slot
		// sessionKey 	- request ID, 0 if it is message
		virtual int sendUserData(DataBufferPtr buffer, int id, int slot, const EndPoint & address = "", size_t flags = SendFlagImmediate) = 0;

		Connection()
		{
			numRefs = 0;
			listener = NULL;
			remoteNode = -1;
		}

		void setEndpointKey(NodeKey newKey)
		{
			remoteNode = newKey;
		}

		NodeKey getEndpointKey() const
		{
			return remoteNode;
		}

		virtual EndPoint getEndpointAddress() const
		{
			return "unknown";
		}

		virtual bool canReach(const EndPoint & pt) const
		{
			return false;
		}

		virtual Transport * getTransport() = 0;

		virtual ~Connection()
		{
			//fprintf(stderr, "Connection::~Connection()\n");
		}
	private:
		/// Non-copyable
		Connection(const Connection &info);
	};

	typedef RefCountedPtr<Connection> ConnectionPtr;

}
