/*
 * node.h
 *
 *  Created on: Nov 26, 2012
 *      Author: vrobot
 */

#pragma once

#include <map>
#include <list>
#include <vector>
#include <set>
#include <string>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <chrono>

#include <nodenet/topic.h>
#include <nodenet/service.h>
#include <nodenet/subscriber.h>
#include <nodenet/publisher.h>

//! Node network objects
namespace nodenet
{
	class PublisherBase;
	class SubscriberBase;
	class Navigator;

	class RequestTicketBase;
	class Node;
	class Transport;

	struct NodeHandlers;

	//! Enumeration for system level request types
	enum SystemRequests
	{
		SystemResponse = 0,
		SystemRequestGreeting,
		SystemRequestEntityList,
		SystemRequestEntityStatus,
		SystemRequestPing,
		SystemSlotsMax = 0xff,
	};

	//! Type to represend request identifier
	typedef uint16_t RequestID;

	//! Root class to control all the entities and transports
	class Node
	{
	public:
		typedef std::set<Transport*> Transports;
		/// Listener is not good concept right now? Maybe replace it later
		/// Should respond to remote topic connections
		class TopicListener
		{
		public:
			virtual void onPublisherFound(Node * node, const char * topicName, Connection * connection) = 0;
			virtual void onPublisherLost(Node * node, const char * topicName, Connection * connection) = 0;
			virtual void onSubscriberFound(Node * node, const char * topicName, Connection * connection) = 0;
			virtual void onSubscriberLost(Node * node, const char * topicName, Connection * connection) = 0;
		};

		Node(const char * nodename);
		~Node();
		
		const char * getName() const;
		Transports getTransports() const;

		//! Set listener for topic callbacks. Deprecated
		void setTopicListener(TopicListener * listener);

		//! Get current listener
		TopicListener * getTopicListener() const;

		/// Should add this one when someone connected
		/// Should remove this one when someone disconnected

		NodeKey key;			//<! the key assigned
		NodeKey nodeKey;		//<! own node key
		TopicListener * topicListener;

		void onClientsUpdated(Topic * topic);

		//! Attach publisher to node
		void attach(PublisherBase * publisher, Topic * topic);
		//! Attach subscriber to node
		void attach(SubscriberBase * subscriber, Topic * topic);

		void detachAllClients();
		void deleteAllEntities();
		/// called by Transport while being detached
		void detachedTransport(Transport * transport);
		/// detach all current transports
		void detachAllTransports();

		/// topic management. Executed in network thread
		//! called when remote node connected to specific topic. We should send him back topic slot id
		void remoteSubscribed(Connection * info, const char * topicName);
		//! Called when data piece is finally received and checked
		int onDataTicketComplete(const DataBufferPtr & buffer, uint16_t id, uint16_t slot, const EndpointDesc & endpoint);
		//! called when transport connected to somewhere.
		void onUserConnectionCreated(Transport * transport, Connection * connection);
		//! connection was closed
		void onUserConnectionClosed(Transport * transport, Connection * connection);
		///// called by transport
		//SlaveInfo * onGreeting(Connection * connection, EndPoint endpoint, const char * name, NodeKey oldKey);
		RequestID storeRequest(Connection * connection, int command, RequestTicketBase * request);

		// remove all pending requests
		void clearRequests();

		Navigator * getNavigator()
		{
			return navigator;
		}

		//! Find entity by its slot number
		EntityPtr getEntity(uint16_t slot);
		//! Find entity by its name
		Entity * findEntity(const char * name);
		//! Find topic by its name
		Topic * findTopic(const char * name);
		//! Find service by its name
		Service * findService(const char * name);

		size_t getTopicsCount() const;
		size_t getTopicsMax() const;

		//! Call all events callbacks
		/*! Iterates through all entities, calling raiseEvents for each
		 */
		int raiseEvents(int waitTimeMS = 0);
		void lock();
		void unlock();

	protected:
		std::string nodename;
		Transports transports;

		Navigator * navigator;
		// lock to access topics list
		std::mutex entityGuard;
		std::condition_variable entityCondition;
		int incomingEvents;

		typedef std::map<RequestID, RequestTicketBase*> RequestStorage;
		RequestStorage requests;	//!< Stored request tickets
		RequestID lastRequestID;	//!< Last request ID. Incremented every new request.
		//! dispatch incoming data object. Called externally by Transport
		int processResponse(const DataBufferPtr & buffer, uint16_t id, const EndpointDesc & endpoint);

		//! Create new topic or use existing
		TopicPtr getTopic(const char * topic);
		//! Create new service or use existing
		ServicePtr getService(const char * name);

		void sendUserData(const EndpointDesc & endpoint, int id, int slot, DataBufferPtr data);

		void writeLog(int level, const char * format, ...);
	private:
		// \addtogroup Handlers. Used in network thread
		//  @{
		// process specific data object type
		int processUserMessage(const DataBufferPtr & buffer, int userSlot);
		int processSystemMessage(const DataBufferPtr & buffer, int userSlot);
		int processUserRequest(const DataBufferPtr & buffer, uint16_t id, uint16_t slot, const EndpointDesc & endpoint);
		int processSystemRequest(const DataBufferPtr & buffer, uint16_t id, uint16_t slot, const EndpointDesc & endpoint);

		// responders for specific system messages
		void respondGreeting(const DataBufferPtr & buffer, int id, const EndpointDesc & endpoint);
		void respondEntityList(const DataBufferPtr & buffer, int id, const EndpointDesc & endpoint);
		void respondEntityStatus(const DataBufferPtr & buffer, int id, const EndpointDesc & endpoint);
		void respondPing(const DataBufferPtr & buffer, int id, const EndpointDesc & endpoint);

		// @}
		enum {MaxSlots = 128};
		typedef std::map<size_t, EntityPtr> EntitySlots;
		EntitySlots entitySlots;
		size_t lastEntitySlot;

		friend class PublisherBase;
		friend class SubscriberBase;
		friend class ServiceProviderBase;
		friend class ServiceClientBase;
		friend class Transport;
		friend struct NodeHandlers;
	};

	class Transport
	{
	public:
		friend class Connection;
		virtual void attach(Node * node) = 0;
		virtual void detach() = 0;

		Transport()
		{
			node = NULL;
		}

		virtual ~Transport()
		{
			fprintf(stderr, "Transport::~Transport()\n");
		}

		// get connection to reach specific address
		virtual Connection * getConnection(const EndPoint & address)
		{
			return NULL;
		}
	protected:
		Node * node;

		int notifyTicketComplete(const DataBufferPtr & buffer, int16_t id, int16_t slot, const EndpointDesc & endpoint)
		{
			if(node)
			{
				return node->onDataTicketComplete(buffer, id, slot, endpoint);
			}
			return -1;
		}


		inline void notifyConnectionCreated(Connection * connection)
		{
			if(node)
			{
				node->onUserConnectionCreated(this, connection);
			}
		}

		inline void notifyConnectionClosed(Connection * connection)
		{
			if(node)
			{
				node->onUserConnectionClosed(this, connection);
			}
		}
	};

	template <class Msg> inline DataBufferPtr writeBuffer(const Msg &msg)
	{
		int size = msg.getEncodedSize();
		if(size > 0)
		{
			DataBufferPtr result = DataBufferPtr::alloc(msg.getEncodedSize());
			msg.encode(result.data(), 0, result.size());
			return result;
		}
		return DataBufferPtr();
	}

} // namespace nodenet
