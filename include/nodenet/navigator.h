/*
 * navigator.h
 *
 *  Created on: Jun 17, 2013
 *      Author: vrobot
 */

#ifndef NAVIGATOR_H_
#define NAVIGATOR_H_

#include <nodenet/node.h>
#include <set>

namespace nodenet
{
/// Network entity description
	struct EntityInfo
	{
		std::string name;	/// full name
		int type;			/// pub, sub, req, rep
		int slot;
		int sources, sinks;

		MessageTypeInfo dataReq, dataRep, dataProcess;
	};

/// Stores network map
	class Navigator
	{
	public:
		/// req, rep, pub, sub

		/// Here master stores all necessary information about slave node
		struct NodeInfo
		{
			std::string name;				/// node name
			NodeKey key;					/// assigned key
			//Connection * connection;		/// "system" connection to slave
			std::set<EndPoint> endpoints;	/// known node endpoints
			std::set<EndPoint> available;	/// checked node endpoints

			long lastCheckTimeMS;			/// list time access was checked

			void addEndpoint(const EndPoint & endPoint);
			void markUnavailable(const EndPoint & endPoint);

			std::map<std::string, EntityInfo> topics;

			EntityInfo & getEntityInfo(const char * name)
			{
				return topics[name];
			}

			bool local;

			const char * getName() const
			{
				return name.c_str();
			}
		};

		NodeKey lastNodeKey;	/// last node key assigned

		Navigator();

		// Do not store returned value for long - all changes should be applied immediately
		NodeInfo * findNodeInfo(NodeKey key);

		NodeInfo * getNodeInfo(const char * nodeName);
		NodeInfo * createNodeInfo();

		/// called when some node responded to ping request
		void nodeResponded(const char * nodeName, long timestamp);
		/// check connection for each known node
		void updateNodeAvailability();

		void enumerateNodes(const char * topic, const char * filter, int pubs, int subs, std::list<EndPoint> & result);

		//int init(const char * address, int port);
		//void update(int duration);

		//int parseRequest(zmq_msg_t & requestData, zmq_msg_t & responseData);

		/*
		/// called by transport
		NodeInfo * onGreeting(Connection * connection, EndPoint endpoint, const char * name, NodeKey oldKey);
		void onMsgGreeting(zmq_msg_t &responseData, const MessageGreetMaster & message);
*/
		/// should notify all topics about this node
		void onSlaveUpdated(NodeInfo * info, size_t subs, size_t pubs);
		/// find
		int enumerateClients(std::list<NodeInfo*> & result, bool pubs, bool subs, NodeInfo * except = NULL) const;
	protected:
		/// Slave nodes block
		typedef std::list<NodeInfo> Slaves;
		Slaves slaves;

		void writeLog(int level, const char * format, ...);
	};
}


#endif /* NAVIGATOR_H_ */
