/*
 * entity.h
 *
 *  Created on: Jul 31, 2013
 *      Author: vrobot
 */

#pragma once

#include <stdint.h>
#include <string>
#include <list>
#include <mutex>
#include <condition_variable>

#include <nodenet/containers.h>
#include <nodenet/connection.h>

namespace nodenet
{
	//! Some forward declarations
	class Node;
	//! Address description to send data ( response ) back
	struct EndpointDesc
	{
		ConnectionPtr connection;
		EndPoint address;

		EndpointDesc()
		{
		}

		EndpointDesc & operator=(const EndpointDesc & desc)
		{
			connection = desc.connection;
			address = desc.address;
			return *this;
		}
		friend bool operator==(const EndpointDesc & a, const EndpointDesc &b)
		{
			return a.address == b.address && a.connection == b.connection;
		}
	};

	//! Link to remote topic slot
	struct RemoteEntityLink
	{
		EndpointDesc endpoint;		//!< connection used to reach it

		uint16_t remoteSlot;		//!< remote slot index
		uint32_t slotKey;			//!< security key
		uint16_t subs;				//!< remote topic subscribers
		uint16_t pubs;				//!< remote topic publishers

		RemoteEntityLink()
		{
			remoteSlot = -1;
			slotKey = 0;
			subs = 0;
			pubs = 0;
		}

		RemoteEntityLink(const RemoteEntityLink &rl)
		{
			endpoint = rl.endpoint;
			remoteSlot = rl.remoteSlot;
			slotKey = rl.slotKey;
			subs = rl.subs;
			pubs = rl.pubs;
		}
		/// check if two link represent the same target
		bool operator==(const RemoteEntityLink & rl) const
		{
			return endpoint == rl.endpoint && remoteSlot == rl.remoteSlot;
		}
	};

	typedef size_t SendFlags_t;

	/// Internal Entity type
	enum EntityType
	{
		EntityUnknown,
		EntityTopic,
		EntityService,
		EntityProcess,
	};

	/// Contains information about network entity, like topic or service.
	///
	class Entity
	{
		friend class Node;
	public:
		Entity() : refs(0), activeIndex(-1), slot(-1) {}
		virtual ~Entity() {}

		//! Set entity name
		/*
		 * Should spawn update event through network
		 */
		void setName(const char * name);
		//! Get entity name
		const char * getName() const;
		/// Get entity internal type
		virtual EntityType getType() const;
		//! get pointer to remote links array
		RemoteEntityLink * getRemoteLinks();
		//! get active remote links count
		size_t getRemoteLinksCount() const;
		//! Add link to remote entity
		void addLink(const RemoteEntityLink & link);
		//! Get sources count
		virtual int getSources() const = 0;
		//! Get sinks count
		virtual int getSinks() const = 0;
		//! executed when connection was closed
		void onLinkClosed(ConnectionPtr remote);
		//! Increment internal refcounter
		int addRef();
		//! Decrement internal refcounter
		int decRef();
		//! Iterate through all clients invoking callbacks. To be refactored awat
		virtual int raiseEvents();
		//! lock data access
		void lock() const;
		//! unlock data access
		void unlock() const;

		//! Check if specified types are compatible
		/*!
		 \param req type info for requests
		 \param rep type info for response
		 \parap proc type info for repeatedly sent progress messages
		 */
		bool isTypeCompatible(MessageTypeInfo * req, MessageTypeInfo * rep, MessageTypeInfo * proc) const;
	protected:
		std::string name;									//!< Entity name
		int refs;											//!< Reference counter
		typedef std::list<RemoteEntityLink> RemoteLinks;
		RemoteLinks remoteLinks;							//!< Links to known remote compatible entities
		size_t activeIndex;									//!< quick list index
		size_t slot;										//!< Assigned topic slot
		std::vector<size_t> localLinks;						//!< local subscriber-publisher links?

		MessageTypeInfo dataReq;							//!< Type info for request messages
		MessageTypeInfo dataRep;							//!< Type info for response messages
		MessageTypeInfo dataProcess;						//!< type info for process messages

		void writeLog(int level, const char * format, ...);
	};

	typedef RefCountedPtr<Entity> EntityPtr;
	/// Reference to network entity, like topic or service.
	/*!
	 * Provides common interface for all the clients
	 */
	enum class ClientEventType
	{
		Invalid,
		FoundProvider,		//!< connected from remote
		Message,			//!< Got message
		Request,			//!< Got request
		Response,			//!< Got response
	};

	//! Stored event for any entity type.
	struct EntityEvent
	{
		ClientEventType type = ClientEventType::Invalid;
		DataBufferPtr data;
		EntityEvent* prev = nullptr;
		EntityEvent* next = nullptr;
		int16_t id = 0;
		int16_t slot = 0;
		EndpointDesc endpoint;
	};

	class EntityClient
	{
	protected:
		mutable std::mutex guard;
		mutable std::condition_variable newData;

	public:
		friend class Node;
		friend class Entity;

		EntityClient();
		virtual ~EntityClient();

		//! Check if attached to any node/entity
		bool valid() const;
		//! Get entity name
		const char * getName() const;
		//! Get assigned node
		Node * getNode() const;
		//! Get assigned entity pointer
		virtual RefCountedPtr<Entity> getEntity() const= 0;
		//! Get current links
		int connections() const;
		//! wait for incoming data or new events
		/// Should be flags for
		/// - Waiting for provider
		/// - Waiting for data
		bool wait(int timeoutMS = 0) const;
		//! Raise all internal callbacks
		virtual int raiseEvents() = 0;
		//! Pop event from list
		bool captureEvent(ClientEventType filter);
		//! Delete captured event
		bool freeEvent();

	protected:
		void notify();
		//! get pending events count
		virtual int hasEvents() const;

		std::list<EntityEvent*> events;

		//LockFree::DLockQueue<EntityEvent> events;

		EntityEvent * currentEvent;		//!< Current processing event

		EntityEvent * addEvent(DataBufferPtr ptr, ClientEventType type, uint16_t id, const EndpointDesc & endpoint);

		void writeLog(int level, const char * format, ...);
	protected:
		mutable Node * node;
	};
}
