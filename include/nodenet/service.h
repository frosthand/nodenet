/*
 * service.h
 *
 *  Created on: May 15, 2013
 *      Author: Dmitry Kargin
 *      dv_frost@mail.ru
 */
#pragma once

#include <nodenet/entity.h>

namespace nodenet
{
	class ServiceClientBase;
	class ServiceProviderBase;

	class Service : public Entity
	{
	public:
		List<ServiceClientBase> clients;
		List<ServiceProviderBase> providers;

		Service();
		~Service();

		int getSources() const;

		int getSinks() const;

		EntityType getType() const;

		// Push request from transport
		int pushRequest(int type, const DataBufferPtr & buffer, uint16_t id, const EndpointDesc & endpoint);
		int sendResponse(DataBufferPtr buffer, int id, int flags);
		// Send request to transport
		int sendRequest(DataBufferPtr buffer, int id, int flags);

		void detachAllClients();
	};

	typedef RefCountedPtr<Service> ServicePtr;

	//! Ticket for ongoing request. Is stored until ticket is complete or canceled
	class RequestTicketBase
	{
	public:
		int refs;
		int slot;	// target slot

		bool sent;
		bool received;
		bool canceled;

		RequestTicketBase()
		{
			refs = 1;
			slot = 0;
			sent = false;
			received = false;
			canceled = false;
		}

		virtual ~RequestTicketBase() {}

		RequestTicketBase * addref()
		{
			refs++;
			return this;
		}

		void release()
		{
			refs--;
			if(refs == 0)
				delete this;
		}

		// fill in response data
		virtual int pushResponse(int type, DataBufferPtr buffer, const EndpointDesc & endpoint) = 0;

		// get type information
		virtual void fillRequestInfo(MessageTypeInfo *  info) = 0;
		virtual void fillResponseInfo(MessageTypeInfo *  info) = 0;
	};

	//! Ticket awaiting for response. Used for system level req/rep
	template<class Request, class Response>
	class RequestTicketSystem : public RequestTicketBase
	{
	public:
		Request request;
		Response response;

		typedef void (*fn_handler_t) (const Request & request, const Response &response, void * userdata, const EndpointDesc & endpoint);

		fn_handler_t handler;
		void * userdata;

		RequestTicketSystem()
		{
			handler = NULL;
			userdata = NULL;
		}

		virtual int pushResponse(int type, DataBufferPtr buffer, const EndpointDesc & endpoint)
		{
			int size = response.decode((void*)buffer.data(), 0, buffer.size());
			if(handler)
				handler(request, response, userdata, endpoint);
			return size;
		}

		virtual void fillRequestInfo(MessageTypeInfo *  info)
		{
			BaseMessageLCM::fillTypeInfo<Request>(info);
		}

		virtual void fillResponseInfo(MessageTypeInfo *  info)
		{
			BaseMessageLCM::fillTypeInfo<Response>(info);
		}
	};

	//! Base class for any service provider
	//! Implements type-independent methods. Not to be used directly
	class ServiceProviderBase : public EntityClient, public ListPart<ServiceProviderBase>
	{
	public:
		ServiceProviderBase(const MessageTypeInfo & req, const MessageTypeInfo & rep);
		~ServiceProviderBase();

		//! Attach to network node
		//! Runs in user thread
		void attach(Node * node, const char * name);
		//! Detach from node
		//! Runs in user thread
		void detach();

		EntityPtr getEntity() const;

		int raiseEvents();
	protected:
		//! Actual implementation for sending response.
		//! User thread
		//! returns 0 if no error, negative otherwise
		int _sendResponse(DataBufferPtr data, int flags);
		MessageTypeInfo typeReq;
		MessageTypeInfo typeRep;

		ServicePtr service;

		friend class Service;
	};

	//! Responds to requests
	template<class Request, class Response> class ServiceProvider: public ServiceProviderBase
	{
	public:
		ServiceProvider(Node * node, const char * name)
			:ServiceProviderBase(GetTypeInfo<Request>(), GetTypeInfo<Response>())
		{
			attach(node, name);
		}

		~ServiceProvider()
		{

		}

		//! try to get request
		bool getRequest(Request & request)
		{
			if(!captureEvent(ClientEventType::Request))
				return false;
			if(request.decode(currentEvent->data.data(), 0, currentEvent->data.size()) == 0)
				return false;
			return true;
		}

		//! send response for active request
		int  pushResponse(Response & response, int flags = 0)
		{
			//1. Pop request event from list
			if(currentEvent == NULL)
				return 0;
			//2. Serialize response
			const size_t dataSize = response.getEncodedSize();

			DataBufferPtr buffer = DataBufferPtr::alloc(dataSize);

			int res = response.encode((void*)buffer.data(), 0, buffer.size());

			if(res < 0)
			{
				fprintf(stderr, "Topic::pushResponse() failed to encode a message");
				return -1;
			}

			//3. Send response
			if(_sendResponse(buffer, flags) != 0)
				return -1;
			return 0;
		}
	protected:
		virtual void handleRequest(const Request & data) {}
	};

	//! Result for sending request
	enum ServiceResult
	{
		ServiceDone,		//!
		ServiceError,		//!< other sort of error
		ServiceTimeout,		//!< dropped request by the timeout
		ServiceDropped,		//!< request was skipped for some reason
		ServiceNoProvider,	//!< no provider was found
	};

	//! Client for remote service.
	//! Can process only one request in a moment.
	class ServiceClientBase : public EntityClient, public ListPart<ServiceClientBase>
	{
		MessageTypeInfo typeReq;
		MessageTypeInfo typeRep;
	public:
		class Ticket;

		ServiceClientBase(const MessageTypeInfo & req, const MessageTypeInfo & rep);
		~ServiceClientBase();

		void attach(Node * node, const char * name);
		void detach();
		//! Cancel ongoing request
		void cancel();

		void waitProvider();

		int raiseEvents()
		{
			assert(false);
			return 0;
		}
	protected:
		EntityPtr getEntity() const;

		bool _sendRequest(DataBufferPtr data, int flags);
	protected:
		bool alive;

		ServicePtr service;
		Ticket * ticket;
	};

	//! Service client.
	//! Works with LCM
	template<class Request, class Response> class ServiceClient: public ServiceClientBase
	{
	public:
		typedef Request request_t;
		typedef Response response_t;
		typedef ServiceClient<request_t, response_t> client_type;

		typedef void (*fn_handler_t) (const Request & request, const Response & response, void * userdata);

		request_t request;
		response_t response;
	protected:
		/// called when ticket is complete
		void onCompleted()
		{
			printf("Request is completed\n");
		}

		struct Callback
		{
			fn_handler_t handler;
			void * userdata;
			Callback()
			{
				handler = NULL;
				userdata = NULL;
			}

			void invoke(const Request & request, const Response & response)
			{
				if(handler)
					handler(request, response, userdata);
			}
		}callback;

	public:
		ServiceClient(Node * node, const char * name)
			:ServiceClientBase(GetTypeInfo<Request>(), GetTypeInfo<Response>())
		{
			currentEvent = NULL;
			pendingMessage = false;
			attach(node, name);
		}

		~ServiceClient()
		{
			alive = false;
		}

		//! Send synchronous request to server
		ServiceResult send(int flags = 0, int timeoutMS = 0)
		{
			if(pendingMessage)
			{
				fprintf(stderr, "ServiceClient::send() - already have unresolved request\n");
				return ServiceError;
			}
			const size_t dataSize = request.getEncodedSize();

			DataBufferPtr buffer = DataBufferPtr::alloc(dataSize);

			if(request.encode((void*)buffer.data(), 0, buffer.size())<0)
			{
				fprintf(stderr, "ServiceClient::send() failed to encode a message\n");
				return ServiceError;
			}
			pendingMessage = true;

			if(!_sendRequest(buffer, flags))
				return ServiceError;

			if(!wait(timeoutMS))
				return ServiceTimeout;

			//! wait until we get some response
			while(true)
			{
				if(wait(timeoutMS))
				{
					int res = decodeResponse();
					if(res == 0)
						return ServiceDone;
					else if(res == -1)
						break;
				}
				else
				{
					cancel();
					return ServiceTimeout;
				}
			}

			return ServiceNoProvider;
		}
	protected:
		bool pendingMessage;
		//! Decodes response from events list
		//! \return -1 if error, 0 if ok
		int decodeResponse()
		{
			assert(pendingMessage == true);

			EntityEvent * event = NULL;

			if(captureEvent(ClientEventType::Response) && currentEvent != NULL)
			{
				pendingMessage = false;
				int res = response.decode(currentEvent->data.data(), 0, currentEvent->data.size());
				freeEvent();
				return res > 0 ? 0 : -1;
			}
			// something bad while decoding response
			return -1;
		}
	};
} // namespace nodenet
