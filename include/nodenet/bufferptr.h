#ifndef _DATA_BUFFER_PTR_H_
#define _DATA_BUFFER_PTR_H_

#include <stdlib.h>
#include <assert.h>

namespace nodenet
{
	/// Placeholder for Interlocked operations
	inline int32_t InterlockedAdd(volatile int32_t * ptr, int32_t val)
	{
		return (*ptr += val);
	}

	int32_t InterlockedCAS(volatile int32_t * ptr, int32_t old_value, int32_t new_value);

	//! Smart pointer to byte data buffer
	//! Uses interlocked reference counter to actual data
	//! Stored data is not alligned. Access it by memcpy.
	class DataBufferPtr
	{
	public:
		//! Destructor
		DataBufferPtr():desc(NULL){}

		//! Default constructor
		DataBufferPtr(const DataBufferPtr &ptr)
		{
			desc = NULL;
			*this = ptr;
		}

		//! Assignment
		DataBufferPtr & operator=(const DataBufferPtr &ptr)
		{
			if(desc != ptr.desc)
			{
				_decref();
				if(ptr.desc != NULL)
				{
				// TODO: atomic addref
					(void)InterlockedAdd(&ptr.desc->dataRefs, 1);
					desc = ptr.desc;
				}
			}
			return *this;
		}

		//! Allocate buffer object of specified size
		static DataBufferPtr alloc(size_t size)
		{
			DataBufferPtr result;
			result.desc = (BufferDesc*)malloc(sizeof(BufferDesc) + size);
			assert(result.desc != NULL);
			result.desc->dataRefs = 1;
			result.desc->dataSize = size;
			return result;
		}

		//! Get data size
		size_t size() const
		{
			return desc == NULL ? 0 : desc->dataSize;
		}

		//! Get const pointer to data
		const char * data() const
		{
			return desc == NULL ? (const char *)desc : (const char*)desc + sizeof(BufferDesc);
		}

		//! Get pointer to data
		char * data()
		{
			return desc == NULL ? (char*)desc : (char*)desc + sizeof(BufferDesc);
		}

		//! Unlink pointer from data
		void reset()
		{
			_decref();
		}

		//! Destructor
		~DataBufferPtr()
		{
			_decref();
		}
	protected:
		void _decref()
		{
			if(desc)
			{
				BufferDesc * p = desc;
				desc = NULL;

				if(InterlockedAdd(&p->dataRefs, -1) == 0)
				{
					free(p);
				}
			}
		}

		struct BufferDesc
		{
			volatile int dataRefs;		// Reference counter
			size_t dataSize;			// Allocated block size
		};

		BufferDesc * desc;
	};

	//! Reference counted pointer to Connection object
	template<class Target>
	class RefCountedPtr
	{
		/// mutable for addRef/decRef methods
		Target * target;
	public:
		RefCountedPtr():target(NULL)	{}

		RefCountedPtr(const RefCountedPtr & p)
		{
			target = p.target;
			if(target)
				target->addRef();
		}

		explicit RefCountedPtr(Target * p)
		:target(p)
		{
			if(target)
				target->addRef();
		}

		~RefCountedPtr()
		{
			decref();
		}

		operator Target*()
		{
			return target;
		}

		operator const Target*()const
		{
			return target;
		}

		const Target * operator->() const
		{
			return target;
		}

		const Target * ptr() const
		{
			return target;
		}

		Target * operator->()
		{
			return target;
		}

		RefCountedPtr & operator=(const RefCountedPtr & p)
		{
			if(target != p.target)
			{
				decref();
				target = p.target;
				if(target)
					target->addRef();
			}
			return *this;
		}

		RefCountedPtr & operator=(Target * p)
		{
			return (*this=RefCountedPtr(p));
		}

		friend bool operator==(const RefCountedPtr & a, const RefCountedPtr & b)
		{
			return a.target == b.target;
		}
	protected:
		void decref()
		{
			if(target != NULL)
			{
				Target * c = target;
				target = NULL;
				if(c->decRef()==0)
				{
					delete c;
				}
			}
		}
	};

}
#endif
