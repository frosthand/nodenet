#pragma once

#include <map>
#include <list>

#include "peer.h"
#include "node.h"

#include <nodenet/navigator.h>

namespace nodenet
{
	using clock = std::chrono::steady_clock;
	using TimePoint = std::chrono::time_point<clock>;

	class TransportIP;

	class ConnectionIP : public Connection
	{
	public:
		typedef RefCountedPtr<ConnectionIP> Ptr;
		ip::Socket socket;
		DataTicket sendingTicket;		/// current ticket being sent

		TransportIP * transport;

		long reconnectTimeoutMax;
		// timestamp of reconnect timeout, seconds
		TimePoint timeoutStart;

		/// TCP messaging protocol
		struct PacketHeaderTCP
		{
			uint32_t totalSize;
			uint16_t targetSlot;
			uint16_t requestId;
		};

		struct PacketHeaderUDP
		{
			uint32_t totalSize;
			uint16_t targetSlot;
			uint16_t requestId;
		};

		DataTicket receivingTicket;

		Transport * getTransport();

		ip::PeerAddress getRemoteAddress() const
		{
			return socket.getRemoteAddress();
		}

		ip::PeerAddress getLocalAddress() const
		{
			return socket.getLocalAddress();
		}

		/// Topics from other side
		ConnectionIP(TransportIP * t)
		{
			reconnectTimeoutMax = -1;
			timeoutStart = clock::now();
			transport = t;
		}

		~ConnectionIP()
		{
			printf("ConnectionIP::~ConnectionIP()\n");
		}

		bool alive() const
		{
			return socket.state != ip::Socket::Empty;
		}

		/// close connection
		void close();

		int sendUserData(DataBufferPtr buffer, int id, int slot, const EndPoint & address, size_t flags);
		int sendSystemData(DataBufferPtr buffer, int id, int slot, const EndPoint & address, size_t flags);

		/// drop all pending data
		void dropPendingData(int * packets, int * bytes);
		/// send all stored data
		void sendPendingData();
	private:
		int sendPendingData(TicketStorage & storage, bool sendAll = false);
		/// Non-copyable
		ConnectionIP(const ConnectionIP &info);
	};

	class TransportIP : public Transport//, public Peer::Callback
	{
		friend class ConnectionIP;

		/// Master connect/reconnect block
		ip::PeerAddress masterAddress;

		char localName[255];		/// local host name
		
		//MessageHandleManager<ConnectionIP *> localDispatcher;
		
		void onRemoteTopicEvent(const char * topic, const ip::PeerAddress & address, int event);
		typedef std::list<ConnectionIP::Ptr> Clients;
		Clients conUsers;			/// active clients list
		
		ip::Socket listenerTCP; 		/// socket for incoming TCP connections
		ip::Socket listenerUDP; 		/// socket for UDP transmission
		int listenerPort;

		TimePoint timeStart;

		ip::Socket conenctionMaster;

		Clients conSlaves;

		ip::SocketSelector_Select selector;

		volatile bool threadShouldStop;
		bool threadActive;

		// guard multithreaded access to sockets
		std::mutex guardNet;
		std::thread updater;

	public:
		TransportIP();
		~TransportIP();

		void attach(Node * node);
		void detach();

		//int initMaster(int port);
		int initSlave(const char * masterAddress, int port);

		int init(int port = 0);

		enum StopFlags
		{
			StopNow,	// stop right now, dropping all the data
			StopWait,	// wait some time to send the rest data
			StopSafe,	// send all data before full stop
		};
		// stop updater thread
		void stop(int flags);
		// start updater thread
		void run();

		void updateOnce(timeval & val);

		/// get assigned TCP/UDP listener port
		int getPort() const;
		/// order to connection to other endpoint
		int link(EndPoint address, int timeoutMS = 0);
		/// is there any user connections
		bool isActive() const;
		/// get total user connections
		size_t peerConnections() const;
		/// clients
		void closeClients();

	protected:
		/// thread function to update internal network
		static void s_updaterThread(TransportIP * transport);
		/// write all sockets to selector
		int writeToSelector(ip::SocketSelector * selector);
		/// check for incoming master connections
		void processIncomingConnections(Clients & newClients);
		/// check for incoming user connections
		void processReceivingTCP(ConnectionIP::Ptr connection);
		/// process client connections
		void process(ConnectionIP::Ptr connection, Clients & newClients);

		void writeLog(int level, const char * format, ...);
		/// from Peer::Callback
	};

	/// environment variable name toyt g override master address
	static const char * envMasterAddress = "NODENET_MASTER";

	class NodeIP : public Node
	{
		/*
		Threading::Mutex netLock;
		Threading::ConditionVariable dataRead;*/
		TransportIP ipnet;
		char masterAddress[255];
		int masterPort;

		Navigator navigator;
	public:
		NodeIP(const char * name, int port = 0);
		~NodeIP();

		const char * getMasterAddress() const;
		int getMasterPort() const;
		bool hasMaster() const;

		bool alive() const
		{
			return ipnet.isActive();
		}

		void updateOnce(int ms);

		int link(const char * endpoint, int timeoutMS = 0)
		{
			return ipnet.link(endpoint, timeoutMS);
		}

		void run()
		{
			ipnet.run();
		}

		void stop(int flags = TransportIP::StopNow)
		{
			ipnet.stop(flags);
		}

	protected:
	};

	EndPoint makeEndpoint(const char * name, int port);

} // namespace nodenet
