#include <assert.h>
#include <stdlib.h>

class RingStream
{
	char * buffer;
	unsigned int maxSize;	/// allocated size
	unsigned int currentSize;
	unsigned int markerRead;		/// current read marker
	unsigned int markerWrite;		/// current write marker
public:

	RingStream(int max)
	{
		buffer = NULL;
		maxSize = 0;
		markerRead = 0;
		markerWrite = 0;
		currentSize = 0;
	}

	~RingStream()
	{
		clear();
	}

	void clear()
	{
		if(buffer)
		{
			delete[]buffer;
			maxSize = 0;
		}
	}



	void resize(size_t newSize)
	{
		if(maxSize == newSize)
			return;
		clear();
		if(newSize == 0)
			return;

		buffer = new char[newSize];
		if(buffer)
		{
			maxSize = newSize;
			markerRead = 0;
			markerWrite = 0;
			currentSize = 0;
		}
	}

	bool overwrites() const
	{
		return true;
	}

	size_t getMaxSize() const
	{
		return maxSize;
	}

	void clean()
	{
		markerWrite = 0;
		markerRead = 0;
		currentSize = 0;
	}

	size_t pop_front(size_t asize)
	{
		if(asize > 0)
		{
			if(asize > currentSize)
				asize = currentSize;
			if(asize + markerRead > maxSize)
				asize = (maxSize - markerRead);
			moveRead(asize);
			currentSize -= asize;
		}
		return asize;
	}
	/// Forced writing. If storage is full - overwrite old data
	int push_back(const char * source, size_t size)
	{
		assert(size <= maxSize);
		/// 1. check border crossing
		if(markerWrite+size > maxSize)
		{
			int firstLength = maxSize - markerWrite;
			memcpy(buffer + markerWrite, source, firstLength);
			memcpy(buffer, source + firstLength, size - firstLength);
		}
		else
		{
			memcpy(buffer + markerWrite, source, size);
		}
		/// move marker
		moveWrite(size);
		/// increase stored size
		currentSize += size;
		if(currentSize > maxSize)
		{
			currentSize = maxSize;
			markerRead = markerWrite;
		}
		return 0;
	}

	/// get stored size
	size_t stored_size() const
	{
		return currentSize;
	}

	/// get first chunk size
	size_t stored_size_plain() const
	{
		if(maxSize - markerRead < currentSize)
			return maxSize - markerRead;
		return currentSize;
	}

	/// get raw pointer to current data
	const char * data() const
	{
		return buffer + markerRead;
	}
/*
	/// Strict data reading. Returns bytes got from buffer
	int read(char * target, int size)
	{
		assert(size <= maxSize);

		if(currentSize == 0)
			return 0;
		// trim size to available
		if(currentSize <= size)
			size = currentSize;
		/// 1. check border crossing
		if(markerRead+size > maxSize)
		{
			int firstLength = maxSize - markerRead;
			memcpy(target, buffer + markerRead, firstLength);
			memcpy(target+firstLength, buffer, size - firstLength);
		}
		else
		{
			memcpy(target, buffer + markerRead, size);
		}

		/// move marker
		markerRead += size;
		if(markerRead >= maxSize)
			markerRead -= maxSize;

		/// increase stored size
		currentSize -= size;
		return size;
	}*/
protected:
	/// move read marker in checking ring borders
	int moveRead(size_t distance)
	{
		assert(distance < maxSize);

		markerRead += distance;
		if(markerRead >= maxSize)
			markerRead -= maxSize;
		return markerRead;
	}
	/// move write marker checking ring borders
	int moveWrite(size_t distance)
	{
		assert(distance < maxSize);
		markerWrite += distance;
		if(markerWrite >= maxSize)
			markerWrite -= maxSize;
		return markerWrite;
	}
};
