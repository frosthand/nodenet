/*
 * Socket for IP transport. Right now TCP is mostly used. Need to implement some reliability for UDP
 */

#pragma once

#include <nodenet/network_ip.h>
#include <vector>

namespace nodenet
{
	namespace ip
	{
		/// Single network slot. Was stripped from frosttools/network.hpp and refactored for standalone usage
		struct Socket
		{
			typedef int SOCKET;

			enum SlotState
			{
				Invalid = -1,
				Empty,
				AcceptingTCP,		/// waiting for connection
				StartConnectingTCP,	/// initiate connection
				ConnectingTCP,		/// connection in progress
				WorkingTCP,
				WorkingUDP,			/// working in the UDP mode
				ReconnectingTCP,
				Dying,
			};

			SlotState state;

			enum SocketType
			{
				SocketInvalid, SocketTCP, SocketUDP, SocketFile
			};

			sockaddr_in target_addr, source_addr;
			size_t slotId;
			SocketType socketType;
			/// Storage for incoming data
			int bytesRecieved;
			size_t bytesTotal;
			unsigned char * buffer;
			size_t bufferLength;
			bool slave;			/// if this socket was created through accept

			/// Events types for poll/select
			enum EventType
			{
				EventIn = 1,
				EventOut = 2,
				EventError = 4,
				EventClosed = 8,
			};

			int events;			/// flags for selected events
			void* userdata;
			int sockfd;

			Socket();
			~Socket();

			int close();
			/// Create socket and assign default settings
			bool create(SocketType socketType);
			/// Start listening mode
			bool listen( int port);

			enum { maxClients = 32 };

			enum ConnectFlags
			{
				ConnectAsync = 0,
				ConnectSync = 1,
			};
			/// Start connection mode. Socket
			bool connect( const PeerAddress * address, size_t flags);
			/// Check if socket has valid file descriptor
			int send(const void * data, size_t size, PeerAddress * address);
			bool validSocket() const;

			/// Get assigned file descriptor
			int getfd() const
			{
				return sockfd;
			}

			/// Get endpoint address
			PeerAddress getRemoteAddress() const;
			PeerAddress getLocalAddress() const;

			/// enable/disable Naggle algorithm
			int setTCPNoDelay(int value);

			/// enable/disable blocking mode
			int setBlocking(int value);

			/// event checkers
			bool shouldEventIn() const
			{
				return state == AcceptingTCP || state == WorkingTCP || state == WorkingUDP;
			}
			bool shouldEventOut() const
			{
				return state == ConnectingTCP || state == WorkingTCP || state == WorkingUDP;
			}
			bool shouldEventError() const
			{
				return state == ConnectingTCP;
			}
			bool hasEvent(EventType type) const
			{
				return (events & type) != 0;
			}

			// Get system error string. Platform specific
			static const char * getStrError();						/// get last error string
			// Get local error. I do not trust it
			static NetErrorType getError();

			/// That's something complicated with this error checking for win32
			//static int getSysErrorCode();
			//static const char * getSysErrorString(int errcode);

			/// free receive buffer
			void releaseBuffer();
			void resizeBuffer(size_t newLength);

			/// Accept connection. For sockets in AcceptingTCP mode
			bool doAccept(Socket & accepted);
			/// Try to connect. For sockets in StartConnectingTCP/ConnectingTCP mode
			bool processTCPConnecting();
			/// Receive data. For sockets in Working/WorkingUDP mode
			bool doReceive(PeerAddress &address);

		protected:
			/// for internal error logging
			void writeLog(int level, const char * format, ...) const {}
		};

		/// Select events for specified socket list
		class SocketSelector
		{
		public:
			virtual ~SocketSelector() {}
			/// add socket to selector
			virtual void add(Socket * sock) = 0;
			/// clear all sockets
			virtual void clear() = 0;
			/// execute check for all stored sockets. Event data will be stored at each Socket::events field
			virtual int check(timeval & timeout) = 0;
		};

		/// Implementation using select
		class SocketSelector_Select : public SocketSelector
		{
		protected:
			fd_set readfds, writefds, exceptfds;
			std::vector<Socket*> sockets;
		public:
			SocketSelector_Select();
			/// add socket to selector
			void add(Socket * sock);
			/// clear all sockets
			void clear();
			/// execute check for all stored sockets. Event data will be stored at each Socket::events field
			int check(timeval & timeout);
		};

	} // namespace ip
}	// namespace nodenet
