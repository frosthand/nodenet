/*
 * navigator.cpp
 *
 *  Created on: Jun 17, 2013
 *      Author: vrobot
 */


#include <stdarg.h>
#include <nodenet/navigator.h>

namespace nodenet
{
	Navigator::Navigator(/*void * context*/)
	{
		lastNodeKey = 0;
	}

	Navigator::NodeInfo * Navigator::findNodeInfo(NodeKey key)
	{
		NodeInfo * result = NULL;
		for(Slaves::iterator it = slaves.begin(); it != slaves.end(); ++it)
		{
			if(it->key == key)
			{
				result = &*it;
				break;
			}
		}
		return result;
	}

	Navigator::NodeInfo * Navigator::createNodeInfo()
	{
		//assert(findNodeInfo(key) == NULL);
		slaves.push_back(NodeInfo());
		NodeInfo &newNode = slaves.back();
		newNode.key = lastNodeKey++;
		newNode.lastCheckTimeMS = 0;
		return &newNode;
	}

	Navigator::NodeInfo * Navigator::getNodeInfo(const char * nodename)
	{
		NodeInfo * result = NULL;
		for(Slaves::iterator it = slaves.begin(); it != slaves.end(); ++it)
		{
			if(it->name == nodename)
				return &(*it);
		}

		result = createNodeInfo();
		result->name = nodename;
		return result;
	}

	void Navigator::nodeResponded(const char * nodeName, long timestamp)
	{
		NodeInfo * node = getNodeInfo(nodeName);
		if(node != NULL)
			node->lastCheckTimeMS = timestamp;
	}

	void Navigator::updateNodeAvailability()
	{
		for(Slaves::iterator it = slaves.begin(); it != slaves.end(); ++it)
		{
			// TODO: implement
		}
	}

	void Navigator::writeLog(int level, const char * format, ...)
	{
		va_list	ap;
		va_start(ap,format);
		const int MAX_LEN=4096;
		static char buffer[MAX_LEN];
#ifdef _MSC_VER
			vsprintf_s(buffer, MAX_LEN,format,ap);
#else
			vsnprintf(buffer, MAX_LEN,format,ap);
#endif

		buffer[MAX_LEN-1]=0;
		printf("Master says: %s\n", buffer);
		va_end(ap);
	}
/*
	int Navigator::init(const char * address, int port)
	{
		char uri[255];
		sprintf(uri, "tcp://*:%d",port);
		//responder = zmq_socket (context, ZMQ_REP);
		//int rc = zmq_bind (responder, uri);
		return 1;
	}
void Navigator::update(int duration)
	{
		zmq_pollitem_t items [] =
		{
			{ responder, 0, ZMQ_POLLIN, 0 },
			//{ subscriber, 0, ZMQ_POLLIN, 0 }
		};
		zmq_poll(items, 1, duration);

		if(items[0].revents & ZMQ_POLLIN )
		{
			zmq_msg_recv (&requestData, responder, 0);
			if(parseRequest(requestData, responseData) < 0)
			{
				zmq_msg_init_data (&responseData, NULL, 0, NULL, NULL);
			}
			zmq_msg_send(&responseData, responder, 0);
		}
	}

	int Navigator::parseRequest(zmq_msg_t & requestData, zmq_msg_t & responseData)
	{
		size_t size = zmq_msg_size(&requestData);

		if(size >= sizeof(Protocol::PacketHeader))
		{
			const char * data = (const char*)zmq_msg_data(&requestData);

			Protocol::PacketHeader header;
			memcpy(&header, data, sizeof(header));

			int totalLength = sizeof(Protocol::PacketHeader) + header.length;
			if(header.length + sizeof(header) != size)
			{
				/// Strange size mismatch
				return -1;
			}
		}
		return 0;
	}

	Navigator::NodeInfo * Navigator::onGreeting(Connection * connection, EndPoint endpoint, const char * nodeName, NodeKey oldKey)
	{
		/// 1. Generate id for slave node
		NodeKey newKey = -1;
		/// try to use existing key (if reconnected?)
		NodeInfo * info = NULL;

		if(oldKey > 0)
		{
			for(Slaves::iterator it = slaves.begin(); it != slaves.end(); ++it)
			{
				if(it->name == nodeName)
				{
					newKey = it->key;
					info = &*it;

					writeLog(0, "Node::onGreeting(%s) warning: reassigned lost node", nodeName);
					break;
				}
			}
		}

		if(newKey == -1)
		{
			newKey = 0;//++lastNodeKey;
			info = this->createNodeInfo(newKey);
		}

		if(info != NULL)
		{
			/// 2. Memorize this node address
			info->name = std::string(nodeName);
			//info->endpoint = endpoint;
			//info->connection = connection;
			info->key = newKey;
			writeLog(0, "Node::onGreeting() %s(%d) greets from %s, assigned key %d", nodeName, oldKey, endpoint.c_str(), newKey);
			/// 3.Send response
			connection->send(0,0, MessageGreetResponse(newKey), SendFlagImmediate);
		}
		else	/// Reject connection
		{
			writeLog(0, "Node::onGreeting() %s(%d) greets from %s, rejected");
			connection->send(0,0, MessageGreetResponse(-1), SendFlagImmediate);
		}
		return info;
	}

	void Navigator::onMsgGreeting(zmq_msg_t &responseData, const MessageGreetMaster & message)
	{
		writeLog(0, "TransportIP::onGreeting()");
		ConnectionIP * connection = NULL;
		ip::PeerAddress address = connection->getRemoteAddress();
		const char * hostname = address.getHostname();
		EndPoint endpoint;
		/// check if address is local
		bool local = false;
		if(strcmp(hostname,"127.0.0.1") == 0)
		{
			/// Local address detected
			writeLog(0, "TransportIP::onGreeting() - local address detected");
			local = true;
		}

		endpoint = makeEndpoint(address.getHostname(), message.data.port);

		NodeInfo * slaveInfo = onGreeting(NULL, endpoint, message.data.nodeName, message.data.nodeKey);

		if(slaveInfo)
			slaveInfo->local = local;
	}

	void Topic::onSlaveUpdated(SlaveInfo * info, size_t subs, size_t pubs)
	{
		SlaveNodes::iterator it = slaveNodes.find(info);
		if(subs + pubs != 0)
		{
			SlaveTopicDesc &desc = slaveNodes[info];
			desc.subs = subs;
			desc.pubs = pubs;
		}
		else if(it != slaveNodes.end())
		{
			slaveNodes.erase(it);
		}
	}
*/
	/*
	int Navigator::enumerateClients(std::list<NodeInfo*> & result, bool pubs, bool subs,  NodeInfo * except) const
	{
		int found = 0;
		for(Slaves::const_iterator it = slaves.begin(); it != slaves.end(); ++it)
		{
			const NodeInfo &desc = (*it);
			if(it->first == except)
				continue;
			if((subs && desc.subs > 0) || (pubs && desc.pubs > 0))
			{
				result.push_back(it->first);
				found++;
			}
		}
		return found;
	}
*/
}
