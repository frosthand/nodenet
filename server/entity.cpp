/*
 * entity.cpp
 *
 *  Created on: Jul 31, 2013
 *      Author: vrobot
 */


#include <nodenet/entity.h>
#include <nodenet/node.h>
#include <stdarg.h>

namespace nodenet
{
	EntityClient::EntityClient()
	{
		node = NULL;
		currentEvent = NULL;
	}

	EntityClient::~EntityClient()
	{

	}

	bool EntityClient::valid() const
	{
		EntityPtr entity = getEntity();
		return node != NULL && entity != NULL;
	}

	Node * EntityClient::getNode() const
	{
		return node;
	}

	const char * EntityClient::getName() const
	{
		EntityPtr entity = getEntity();
		if(entity != NULL)
			return entity->getName();
		return NULL;
	}

	int EntityClient::connections() const
	{
		EntityPtr entity = getEntity();
		if(entity != NULL)
			return entity->getRemoteLinksCount();
		return 0;
	}

	bool EntityClient::wait(int timeoutMS) const
	{
		bool result = true;
		//printf("Entity::wait() - Checking for new events\n");
		// TODO: Implement more robust event cycle
		do
		{
			std::unique_lock<std::mutex> lock(guard);
			if (!hasEvents())
				break;
			//printf("Entity::wait() - Wait for new events\n");
			if(timeoutMS > 0)
			{
				std::cv_status wr = newData.wait_for(lock, std::chrono::milliseconds(timeoutMS));
				if(wr != std::cv_status::timeout)
				{
					result = false;
					break;
				}
			}
			else
			{
				newData.wait(lock);
			}
		}while(true);
		return result;
	}

	void EntityClient::notify()
	{
		newData.notify_all();
	}

	EntityEvent * EntityClient::addEvent(DataBufferPtr ptr, ClientEventType type, uint16_t id, const EndpointDesc & endpoint)
	{
		//writeLog(0, "EntityClient::addEvent()");
		EntityEvent * result = new EntityEvent();
		result->data = ptr;
		result->next = NULL;
		result->type = type;
		result->id = id;
		result->endpoint = endpoint;
		events.push_back(result);
		// Notify any waiters

		newData.notify_one();
		writeLog(0, "EntityClient::addEvent() - done\n");
		return result;
	}

	int EntityClient::hasEvents() const
	{
		return !events.empty();
	}

	bool EntityClient::captureEvent(ClientEventType filter)
	{
		if(currentEvent != NULL || !hasEvents())
			return false;

		EntityEvent* event = nullptr;
		while(true)
		{
			//if(!events.pop(event))
			//	return false;
			if (events.empty())
				return false;
			event = events.front();
			events.pop_front();

			if(event->type == filter)
			{
				currentEvent = event;
				return true;
			}
			else
			{
				delete event;
			}
		}
		return false;
	}

	bool EntityClient::freeEvent()
	{
		EntityEvent * event = currentEvent;
		if(currentEvent != NULL)
		{
			currentEvent = NULL;
			delete event;
			return true;
		}
		return false;
	}

	void EntityClient::writeLog(int level, const char * format, ...)
	{
		va_list	ap;
		va_start(ap,format);

		{
			const int MAX_LEN=4096;
			static char buffer[MAX_LEN];
		#ifdef _MSC_VER
				vsprintf_s(buffer, MAX_LEN,format,ap);
		#else
				vsnprintf(buffer, MAX_LEN,format,ap);
		#endif

			buffer[MAX_LEN-1]=0;
			printf("EntityClient: %s\n", buffer);
		}
		va_end(ap);
	}
	/////////////////////////////////////////////////////////////////////////////////
	void Entity::setName(const char * name)
	{
		this->name = name;
	}

	const char * Entity::getName() const
	{
		return name.c_str();
	}

	EntityType Entity::getType() const
	{
		return EntityUnknown;
	}

	size_t Entity::getRemoteLinksCount() const
	{
		return remoteLinks.size();
	}

	void Entity::addLink(const RemoteEntityLink & link)
	{
		/// 1. Check if simular link already exists ( name/slot pair )
		/// 2. Update key
		remoteLinks.push_back(link);
	}

	void Entity::onLinkClosed(ConnectionPtr connection)
	{
		writeLog(1, "Entity::onLinkClosed()");
		for(RemoteLinks::iterator it = remoteLinks.begin(); it != remoteLinks.end();)
		{
			RemoteLinks::iterator current = it++;
			if(current->endpoint.connection == connection)
			{
				remoteLinks.erase(current);
			}
		}
	}

	int Entity::raiseEvents()
	{
		return 0;
	}

	int Entity::addRef()
	{
		return ++refs;
	}
	//! Decrement internal refcounter
	int Entity::decRef()
	{
		return --refs;
	}

	void Entity::lock() const
	{
		// TODO: implement
	}

	void Entity::unlock() const
	{
		// TODO: implement
	}

	void Entity::writeLog(int level, const char * format, ...)
	{
		va_list	ap;
		va_start(ap,format);

		{
			const int MAX_LEN=4096;
			static char buffer[MAX_LEN];
		#ifdef _MSC_VER
				vsprintf_s(buffer, MAX_LEN,format,ap);
		#else
				vsnprintf(buffer, MAX_LEN,format,ap);
		#endif

			buffer[MAX_LEN-1]=0;
			printf("Entity: %s\n", buffer);
		}
		va_end(ap);
	}
}

