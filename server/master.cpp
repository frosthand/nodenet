#include <nodenet/node.h>
#include <nodenet/transport_ip.h>
#include <frosttools/threads.hpp>

#ifdef WIN32
#else
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#endif

void testAddress()
{
	hostent * he = NULL;
	struct in_addr ipv4addr;
	struct in6_addr ipv6addr;

	inet_pton(AF_INET,"192.0.2.34",&ipv4addr);
	he = gethostbyaddr((const char*)&ipv4addr, sizeof(ipv4addr), AF_INET);
	if(he != NULL)
		printf("gethostbyaddr ip4 name: %s\n",he->h_name);

	inet_pton(AF_INET6,"2001:db8:63b3:1::beef",&ipv4addr);
	he = gethostbyaddr((const char*)&ipv6addr, sizeof(ipv6addr), AF_INET6);
	if(he != NULL)
		printf("gethostbyaddr ip6 name: %s\n",he->h_name);

	/// Old good?
	char hostname[255];
	int len = gethostname(hostname, sizeof(hostname));
	he = gethostbyname(hostname);
	if(he != NULL)
	{
		struct in_addr **addr_list;
		printf("Official name is: %s\n", he->h_name);
		printf("IP addresses: ");
		addr_list = (struct in_addr**)he->h_addr_list;

		for(int i = 0; addr_list[i] != NULL; i++)
		{
			printf("%s ", inet_ntoa(*addr_list[i]));
		}
		printf("\n");
		//result.setHostname(inet_ntoa(*(struct in_addr*)he->h_addr));
	}
}

using namespace nodenet;

bool runMaster = true;

int main(int argc, char *argv[])
{
	//testAddress();
	Node node("master");
	TransportIP ipnet;

	int port = 10113;
	if(argc == 2)
	{
		port = atoi(argv[1]);
	}

	ipnet.attach(&node);
	/*
	if(ipnet.initMaster(port))
		return -1;
*/

	while(runMaster)
	{
		/// update tcp network
		timeval time = {0, 10000};
		ipnet.updateOnce(time);
		Threading::sleep(100);
	}

	return 0;
}
