//#include "stdafx.h"
#include <nodenet/transport_ip.h>

#include <stdlib.h>
#include <string.h>

#include <assert.h>
#include <stdarg.h>
#include <stdio.h>

#include <ctype.h>
#include <chrono>

#include "mastermsg/GreetMsg.hpp"

#ifdef WIN32
#define snprintf _snprintf
#else
#include <netdb.h>
#endif

namespace nodenet
{
	EndPoint makeEndpoint(const char * name, int port)
	{
		char buffer[255];
		snprintf(buffer, 255, "ip:%s:%d", name, port);
		return EndPoint(buffer);
	}

	bool fillEndPoint(ip::PeerAddress & address, const char * str)
	{
		char tmp[255];
		strncpy(tmp, str, 255);
		const char *suffix_part = strtok(tmp, ":");
		const char *host_part = strtok(NULL, ":");
		const char *port_part = strtok(NULL, "\n");
		if(suffix_part == NULL || port_part == NULL || strcmp(suffix_part, "ip") != 0 || !isdigit(*port_part))
			return false;
		address.setHostname(host_part);
		address.setPort(atoi(port_part));

		return true;
	}
	/////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////
	TransportIP::TransportIP()
	{
		threadShouldStop = false;
		threadActive = false;

		listenerPort = -1;
		timeStart = clock::now();
		//timer.start();
	}

	TransportIP::~TransportIP()
	{
		stop(StopNow);
		listenerTCP.close();
		detach();
	}

	void TransportIP::detach()
	{
		if(node != NULL)
		{
			// TODO: notify node about closed connections
			// actually they would be opened, but not accessible by node
			closeClients();
			node = NULL;
		}
	}

	void TransportIP::attach(Node * node)
	{
		if(this->node != node)
			detach();

		this->node = node;
	}

	bool TransportIP::isActive() const
	{
		return conUsers.size() > 0;
	}

	int TransportIP::link(EndPoint endpoint, int timeoutMS)
	{
		guardNet.lock();
		ConnectionIP::Ptr connection(new ConnectionIP(this));
		connection->reconnectTimeoutMax = timeoutMS;
		ip::PeerAddress address;//(endpoint);

		// place socket to connection mode
		if(fillEndPoint(address, endpoint.c_str()))
		{
			if(connection->socket.connect(&address, 0))
			{
				conUsers.push_back(connection);
			}
			else
			{
				writeLog(0, "TransportIP::link(%s) : failed to initialize connection\n", endpoint.c_str());
			}
		}
		else
		{
			writeLog(0, "TransportIP::link(%s) : failed to parse address\n", endpoint.c_str());
		}
		guardNet.unlock();

		return 0;
	}

	/// Network handlers

	void TransportIP::closeClients()
	{
		guardNet.lock();
		while(!conUsers.empty())
		{
			conUsers.front()->close();
		}
		guardNet.unlock();
		/*
		/// TODO: implement correct client closing
		for(Clients::iterator it = clients.begin(); it != clients.end(); ++it)
		{
			ConnectionIP * info = *it;
			net->close(info->tcpSlot);
			delete info;
		}
		clients.clear();*/
	}
#ifdef USE_DEPRECATED
	/// got connection from remote
		bool TransportIP::onIncomingConnection(Peer * peer, Peer::SlotHandle listenerTCP, Peer::SlotHandle handle)
		{
			ConnectionIP * connection = (ConnectionIP*)peer->getSlotUserdata(listenerTCP);
			assert(connection != NULL);

			connection->notifyConnectionCreated();
	/*
			/// if someone was trying to connect to our master
			if(connection->type == ConnectionIP::ConnectionMasterListen)
			{
				writeLog(0 ,"TransportIP::onIncomingConnection(listen=%d,handle=%d) incoming slave connection", listener, handle);
				ConnectionIP * slave = createConnection(ConnectionIP::ConnectionToSlave, handle);
				notifyConnectionCreated(slave);
			}
			else if(connection->type == ConnectionIP::ConnectionUserListen)
			{
				writeLog(0 ,"TransportIP::onIncomingConnection(listen=%d,handle=%d) incoming user connection", listener, handle);
				ConnectionIP * user = createConnection(ConnectionIP::ConnectionUser, handle);
				notifyConnectionCreated(user);
			}
			else
			{
				writeLog(0 ,"TransportIP::onIncomingConnection(listen=%d,handle=%d) strange connection type", listener, handle);
				return false;
			}*/
			return true;	// accept all by default
		}

		void TransportIP::onSlotReleased(Peer * peer, Peer::SlotHandle handle)
		{
			writeLog(0, "TransportIP::onSlotReleased(handle=%d)", handle);
		}

		void TransportIP::onSlotClosed(Peer * peer, Peer::SlotHandle handle)
		{
			writeLog(0, "TransportIP::onSlotClosed(handle=%d)", handle);

			ConnectionIP * connection = (ConnectionIP*)peer->getSlotUserdata(handle);
			assert(connection != NULL);

			/*bool reconnectMaster = false;

			if(connection->type == ConnectionIP::ConnectionToMaster)
			{
				if(masterState == MasterReady)
					reconnectMaster = true;
				else if(masterState == MasterConnecting)
				{
					startMasterTimeout();
				}
			}*/

			if(!connection->alive())
				connection->close();		/// remove node from targets
			/// remove node from sources

			/*
			/// reconnect to lost master
			if(reconnectMaster)
			{
				masterState = MasterNull;
				startMasterConnect();
			}*/
		}
	/// connected to remote
	void TransportIP::onSlotConnected(Peer * peer, Peer::SlotHandle handle)
	{
		ConnectionIP * connection = (ConnectionIP*)peer->getSlotUserdata(handle);
		assert(connection != NULL);
		assert(node != NULL);

		writeLog(0, "Node::onConnected(%d) to user", handle);

		notifyConnectionCreated(connection);
		//this->
	}
	unsigned int TransportIP::onRecieve(Peer * peer, Peer::SlotHandle handle, const void * data, unsigned int length, PeerAddress * address)
	{
		//writeLog(0,"TransportIP::onRecieve");
		ConnectionIP * connection = (ConnectionIP*)peer->getSlotUserdata(handle);
		assert(connection != NULL);
		size_t consumed = 0;

		while(consumed < length)
		{
			if(sizeof(Protocol::PacketHeader) + consumed > length)
				break;

			Protocol::PacketHeader * header = (Protocol::PacketHeader*)((char*)data + consumed);
			int totalLength = sizeof(Protocol::PacketHeader) + header->length;
			if(header->length + sizeof(Protocol::PacketHeader) + consumed > length)
				break;

			dispatchMessage(connection, header->targetId, header->type, (const char*) data + consumed + sizeof(Protocol::PacketHeader), header->length);

			consumed += sizeof(Protocol::PacketHeader) + header->length;
		}
		return consumed;
	}
#endif

/*
	void TransportIP::onMasterMsgLinkUser(ConnectionIP * connection, const MessageLinkUser & message)
	{
		ip::PeerAddress address;

		if(!fillEndPoint(address, message.data.endpoint))
		{
			writeLog(0, "TransportIP::onMsgLinkUser(%s) - failed to fill address from endpoint", message.data.endpoint);
			return;
		}

		if(message.data.flags & DataLink::LinkConnectionAddr)
		{
			ip::PeerAddress remoteAddress = connection->getRemoteAddress();
			const char * hostName = remoteAddress.getHostname();
			writeLog(0, "TransportIP::onMsgLinkUser(%s) - using connection address %s instead", message.data.endpoint, hostName);
			address.setHostname(remoteAddress.getHostname());
		}

		/// 1. Check if this node is already connected
		for(Clients::const_iterator it = conUsers.begin(); it != conUsers.end(); ++it)
		{
			if((*it)->getEndpointKey() == message.data.nodeKey)
			{
				writeLog(0, "TransportIP::onMsgLinkUser(%s) - node was already linked", message.data.endpoint);
				return;
			}
		}

		writeLog(0, "TransportIP::onMsgLinkUser(%s)", message.data.endpoint);
		//net->lock();

		ConnectionIP * newLink = createConnection();
		newLink->socket.connect(&address, ip::Socket::ConnectAsync);
	}
*/
	const int nameMasterPort = 10114;

	int TransportIP::init(int port)
	{
		if(node == NULL)
			return -1;

		if(listenerTCP.validSocket())
			listenerTCP.close();

		listenerPort = port;

		if(!listenerTCP.listen(port))
		{
			listenerPort = -1;
			return -1;
		}

		ip::PeerAddress address(listenerTCP.getLocalAddress());
		int assignedPort = address.getPort();
		writeLog(0,"TransportIP::initUser() binded to %d", assignedPort);

		return 0;
	}

	int TransportIP::getPort() const
	{
		return listenerPort;
	}

	int TransportIP::writeToSelector(ip::SocketSelector * selector)
	{

		selector->add(&listenerTCP);
		for(Clients::iterator it = conUsers.begin(); it != conUsers.end(); it++)
		{
			selector->add(&(*it)->socket);
		}
/*
		selector->add(&masterListen);
		for(Clients::iterator it = conSlaves.begin(); it != conSlaves.end(); it++)
		{
			selector->add(&(*it)->socket);
		}*/

		return 0;
	}

	void TransportIP::processReceivingTCP(ConnectionIP::Ptr connection)
	{
		ip::Socket & s = connection->socket;
		ip::PeerAddress address;
		if(!s.doReceive(address))
			return;

		address = s.getRemoteAddress();
		size_t processed = 0;

		DataTicket & ticket = connection->receivingTicket;

		if(s.bytesRecieved - processed > 0)
			writeLog(1, "TransportIP::processReceivingTCP() - %d of %d bytes to process", s.bytesRecieved - processed, s.bytesRecieved);

		while(s.bytesRecieved - processed > 0)
		{
			if(ticket.transmitState == DataTicket::StateEmpty)
			{
				ticket.transmitState = DataTicket::StateHeader;
			}
			// do we have header?
			if(ticket.transmitState == DataTicket::StateHeader)
			{
				ConnectionIP::PacketHeaderTCP header;
				if(s.bytesRecieved - processed >= sizeof(header))
				{
					memcpy(&header, s.buffer + processed, sizeof(header));
					ticket.requestId = header.requestId;
					ticket.targetSlot = header.targetSlot;
					ticket.buffer = DataBufferPtr::alloc(header.totalSize);
					ticket.dataProcessed = 0;
					ticket.transmitState = DataTicket::StateData;
					processed += sizeof(header);
				}
				else
					break;
			}
			else if(ticket.transmitState == DataTicket::StateData)
			{
				int remaining = ticket.buffer.size() - ticket.dataProcessed;
				int toRead = 0;
				if(s.bufferLength - processed >= remaining )
					toRead = remaining;
				else
					toRead = s.bufferLength - processed;

				memcpy(ticket.buffer.data() + ticket.dataProcessed, s.buffer + processed, toRead);
				ticket.dataProcessed += toRead;
				processed += toRead;

				// if ticket is done
				if(ticket.remaining() == 0)
				{
					EndpointDesc desc;
					desc.address = makeEndpoint(address.getHostname(), address.getPort());
					desc.connection = ConnectionPtr(connection);
					notifyTicketComplete(ticket.buffer, ticket.requestId, ticket.targetSlot, desc);
					ticket.reset();
				}
			}
		}
		assert(processed <= s.bytesRecieved && processed >= 0);
		assert(s.bytesRecieved >= 0);
		int unreceivedData = s.bytesRecieved - processed;
		if(unreceivedData > 0)
			memmove(s.buffer, s.buffer + processed, unreceivedData);
		assert(unreceivedData >= 0);
		s.bytesRecieved = unreceivedData;
	}

	void TransportIP::process(ConnectionIP::Ptr connection, Clients & newClients)
	{
		ip::Socket & s = connection->socket;
		if(s.state == ip::Socket::StartConnectingTCP)
		{

			ip::PeerAddress localAddress = connection->getLocalAddress();
			if(s.processTCPConnecting())
			{
				ip::PeerAddress remoteAddress = connection->getRemoteAddress();
				writeLog(0, "Connected to %s:%d from %s:%d",remoteAddress.getHostname(), remoteAddress.getPort(), localAddress.getHostname(), localAddress.getPort());
				notifyConnectionCreated(connection);
			}
			else if(s.state == ip::Socket::Dying)
			{
				ip::PeerAddress remoteAddress = connection->getRemoteAddress();
				writeLog(0, "Failed to connect to %s:%d from %s:%d",remoteAddress.getHostname(), remoteAddress.getPort(), localAddress.getHostname(), localAddress.getPort());

				if(connection->reconnectTimeoutMax > 0)
				{
					writeLog(0, "Initiating reconnect");
					s.state = ip::Socket::ReconnectingTCP;
					connection->timeoutStart = clock::now();
				}
			}
		}
		else if (s.hasEvent(ip::Socket::EventOut) && s.state == ip::Socket::ConnectingTCP)
		{

			ip::PeerAddress localAddress = connection->getLocalAddress();
			if(s.processTCPConnecting())
			{
				ip::PeerAddress remoteAddress = connection->getRemoteAddress();
				writeLog(0, "Connected to %s:%d from %s:%d",remoteAddress.getHostname(), remoteAddress.getPort(), localAddress.getHostname(), localAddress.getPort());
				notifyConnectionCreated(connection);
			}
			else if(s.state == ip::Socket::Dying)
			{
				ip::PeerAddress remoteAddress = connection->getRemoteAddress();
				writeLog(0, "Failed to connect to %s:%d from %s:%d",remoteAddress.getHostname(), remoteAddress.getPort(), localAddress.getHostname(), localAddress.getPort());
				if(connection->reconnectTimeoutMax > 0)
				{
					writeLog(0, "Initiating reconnect");
					s.state = ip::Socket::ReconnectingTCP;
					connection->timeoutStart = clock::now();
				}
			}
		}
		else if (s.hasEvent(ip::Socket::EventIn))
		{
			if (s.state == ip::Socket::AcceptingTCP)
			{
				ConnectionIP::Ptr newConnection(new ConnectionIP(this));
				if(s.doAccept(newConnection->socket))
				{
					newClients.push_back(newConnection);
					notifyConnectionCreated(connection);
					ip::PeerAddress remoteAddress = newConnection->getRemoteAddress();
					this->writeLog(0, "Got incoming connection from %s:%d", remoteAddress.getHostname(), remoteAddress.getPort());
				}
				else
				{
					writeLog(0, "Failed to accept connection");
				}
			}
			else if (s.state == ip::Socket::WorkingTCP)
			{
				processReceivingTCP(connection);
			}
			else if (s.state == ip::Socket::WorkingUDP)
			{
				// TODO: design UDP protocol
				//processReceiving(connection);
			}
		}
		else if(s.state == ip::Socket::ReconnectingTCP)
		{
			auto currentTime = clock::now();
			auto delta = currentTime - connection->timeoutStart;
			if(delta >= std::chrono::milliseconds(connection->reconnectTimeoutMax))
			{
				s.state = ip::Socket::ConnectingTCP;
			}
		}

		if (!s.validSocket())
		{
			s.state = ip::Socket::Empty;
			this->writeLog(0, "Closed socket");
		}
	}

	void TransportIP::s_updaterThread(TransportIP * transport)
	{
		assert(transport->threadActive == false);
		transport->threadActive = true;

		while(!transport->threadShouldStop)
		{
			timeval timeout;
			timeout.tv_sec = 0;
			timeout.tv_usec = 1000;
			transport->updateOnce(timeout);
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
		}
	}

	void TransportIP::processIncomingConnections(Clients & newClients)
	{
		ip::Socket &s = listenerTCP;

		if (s.hasEvent(ip::Socket::EventIn) && s.state == ip::Socket::AcceptingTCP)
		{
			ConnectionIP::Ptr newConnection(new ConnectionIP(this));
			if(s.doAccept(newConnection->socket))
			{
				newClients.push_back(newConnection);
				notifyConnectionCreated(newConnection);
				ip::PeerAddress remoteAddress = newConnection->getRemoteAddress();
				this->writeLog(0, "Got incoming connection from %s:%d", remoteAddress.getHostname(), remoteAddress.getPort());
			}
			else
			{
				writeLog(0, "Failed to accept connection");
			}
		}

		if (!s.validSocket())
		{
			s.state = ip::Socket::Empty;
			this->writeLog(0, "Main TCP listener socket became invalid");
		}
	}
	/// TCP transport specific
	void TransportIP::updateOnce(timeval & timeout)
	{
		this->guardNet.lock();
		// 1. update network (read data)
		selector.clear();
		writeToSelector(&selector);
		selector.check(timeout);
		//processMaster();
		//processUser();
		//processClients();

		Clients newClients;

		// 0. Process incoming data
		processIncomingConnections(newClients);
		// 1. Process active clients
		for(Clients::iterator it = conUsers.begin(); it != conUsers.end(); it++)
			process(*it, newClients);
		// 2. Send pending data
		for(Clients::iterator it = conUsers.begin(); it != conUsers.end(); it++)
		{
			ConnectionIP * connection = (*it);
			connection->sendPendingData();
		}
		// 3. Remove dead clients
		for(Clients::iterator it = conUsers.begin(); it != conUsers.end();)
		{
			ConnectionIP * connection = *it;
			Clients::iterator toDelete = it++;
			if(connection->socket.state == ip::Socket::Dying || connection->socket.state == ip::Socket::Empty)
			{
				writeLog(0,"Removing dead socket");
				notifyConnectionClosed(connection);
				conUsers.erase(toDelete);
			}
		}

		// 4. Add new clients
		conUsers.merge(newClients);

		guardNet.unlock();
	}
/*
	void TransportIP::onNewConnection(ConnectionIP * connection)
	{
		RequestTicket<mastermsg::RequestEntityList, mastermsg::RequestEntityList> * ticket = new RequestTicket<mastermsg::RequestEntityList, mastermsg::RequestEntityList>();

		ticket->request.filter="*";
		RequestID id = node->storeRequest(connection, SystemRequestEntityList, ticket);

		int size = ticket->request.getEncodedSize();
		DataBufferPtr buffer = writeBuffer(ticket->request);
		connection->sendData(buffer, id, SystemRequestEntityList, 0);
	}
*/
	void TransportIP::stop(int flags)
	{
		threadShouldStop = true;
		updater.join();
	}

	void TransportIP::run()
	{
		updater = std::thread(s_updaterThread, this);
	}

	size_t TransportIP::peerConnections() const
	{
		return conUsers.size();
	}

	void TransportIP::writeLog(int level, const char * format, ...)
	{
		va_list	ap;
		va_start(ap,format);
		const int MAX_LEN=4096;
		static char buffer[MAX_LEN];
//#ifdef _MSC_VER
//		vsprintf_s(buffer, MAX_LEN,format,ap);
//#else
		vsnprintf(buffer, MAX_LEN,format,ap);
//#endif

		buffer[MAX_LEN-1]=0;
		printf("Node %s says: %s\n", node->getName(), buffer);
		va_end(ap);
	}
	////////////////////////////////////////////////////////////////////////////////////
	// NodeIP Helper
	////////////////////////////////////////////////////////////////////////////////////
	NodeIP::NodeIP(const char * name, int port)
	:Node(name)
	{
		ipnet.attach(this);

		strcpy(masterAddress, "127.0.0.1");
		masterPort = 10113;

		ipnet.init(port);

		const char * env_master = getenv(envMasterAddress);
		if(env_master != NULL)
		{
			char tmp[255];
			strncpy(tmp, env_master, 255);
			const char *host_part = strtok(tmp, ":");
			const char *port_part = strtok(NULL, "\n");

			if(host_part && port_part && isdigit(*port_part))
			{
				strncpy(masterAddress, host_part, sizeof(masterAddress));
				masterPort = atoi(port_part);
			}
		}
		//connectMaster(masterAddress, masterPort);
	}

	NodeIP::~NodeIP()
	{

	}

	const char * NodeIP::getMasterAddress() const
	{
        return masterAddress;
	}

	int NodeIP::getMasterPort() const
	{
        return masterPort;
    }

	bool NodeIP::hasMaster() const
	{
		return false;//connectionMaster != NULL;
	}

	void NodeIP::updateOnce(int ms)
	{
		timeval time = {0, ms*1000};
		ipnet.updateOnce(time);
	}
}
