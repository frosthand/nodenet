/*
 * connection.cpp
 *
 *  Created on: Feb 1, 2013
 *      Author: vrobot
 */


#include <nodenet/connection.h>

#include <nodenet/node.h>

namespace nodenet
{

#ifdef USE_DEPRECATED
	int Connection::sendMessage(uint16_t slot, uint32_t key, const BaseMessageLCM & msg, size_t flags)
	{
		/// 1. Init header
		Protocol::PacketHeader header;// = *(Protocol::PacketHeader*)data;
		header.length = msg.getSize();
		header.type = msg.getType();
		header.targetId = slot;
		header.key = key;

		/*if(flags |= SendFlagImmediate)
		{
			// 1. Send last message part
			// 2. Send this message, permitting blocking operation
		}
		else*/
		{
			// Use membuffer to send
			size_t availableSize = ring.getMaxSize() - ring.stored_size();
			// 1. Check available size
			if(availableSize < (header.length+sizeof(Protocol::PacketHeader)))
			{
				/// cannot store message - can we drop it?
				if(flags & SendFlagCanDrop)
					return 0;
				else
				{
					/// TODO: wait here for
				}
			}
			else
			{
				//ring<<header;
				//ring<<msg;

				// store message to ring
				this->write(&header, sizeof(header));
				msg.write(this);
				return header.length;
			}			
		}
		return 0;
	}
#endif
}
