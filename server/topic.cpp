/*
 * topic.cpp
 *
 *  Created on: Feb 1, 2013
 *      Author: vrobot
 */

#include <nodenet/subscriber.h>
#include <nodenet/publisher.h>
#include <nodenet/topic.h>

#include <stdio.h>

namespace nodenet
{
	////////////////////////////////////////////////////////////////////////////////
	// Node::Topic
	////////////////////////////////////////////////////////////////////////////////
	Topic::Topic()
	{
		activeIndex = 0;
		accessKey = 0;
	}

	Topic::~Topic()
	{
		printf("Topic::~Topic()\n");
		detachAllClients();
	}

	void Topic::detachAllClients()
	{
		while(subscribers.head != NULL)
		{
			SubscriberBase * obj = subscribers.head;
			obj->detach();	/// should also remove it from list
		}

		while(publishers.head != NULL)
		{
			PublisherBase * obj = publishers.head;
			obj->detach();	/// should also remove it from list
		}
	}

	// Publish message to all local or remote subscribers
	// TCP based:
	// For each link related to this topic
	//		send data through this link

	// UDP based:
	// For each subscribed address
	//		send data to that address
	void Topic::publishMessage(const BaseMessageLCM & msg, SendFlags_t flags)
	{
		RemoteEntityLink * link = getRemoteLinks();
		const size_t dataSize = msg.getEncodedSize();

		DataBufferPtr buffer = DataBufferPtr::alloc(dataSize);

		if(msg.encode((void*)buffer.data(), 0, buffer.size())<0)
		{
			fprintf(stderr, "Topic::publishMessage() failed to encode a message");
			return;
		}

		for(size_t i = 0; i < getRemoteLinksCount(); i++, link++)
		{
			// TODO: replace on Node::sendUserData, to wrap threading in a better way
			Connection * connection = link->endpoint.connection;
			connection->sendUserData( buffer, 0, link->slotKey + SystemSlotsMax, link->endpoint.address, flags);
		}
		/// TODO: 2. Send to local links
	}

	RemoteEntityLink * Entity::getRemoteLinks()
	{
		if(getRemoteLinksCount() == 0)
			return NULL;
		return &remoteLinks.front();
	}
	//////////////////////////////////////////////////////////////////////////
	int Topic::pushMessage(int type, const DataBufferPtr & buffer)
	{
		const char * data = buffer.data();
		size_t length = buffer.size();
		int result = 0;

		for(SubscriberBase * subscriber = subscribers.head; subscriber != NULL; subscriber = subscriber->next)
		{
			if(subscriber->pushMessage(type, data, length))
				result++;
		}

		return result;
	}

	int Topic::raiseEvents()
	{
		int counter = 0;

		for(SubscriberBase * subscriber = subscribers.head; subscriber != NULL; subscriber = subscriber->next)
		{
			counter+=subscriber->raiseEvents();
		}

		return counter;
	}

	size_t Topic::getRemotePubs() const
	{
		size_t result = 0;

		for(RemoteLinks::const_iterator it = remoteLinks.begin(); it != remoteLinks.end(); ++it)
		{
			const RemoteEntityLink & link = *it;
			result += link.pubs;
		}
		return result;
	}

	size_t Topic::getRemoteSubs() const
	{
		size_t result = 0;

		for(RemoteLinks::const_iterator it = remoteLinks.begin(); it != remoteLinks.end(); ++it)
		{
			const RemoteEntityLink & link = *it;
			result += link.subs;
		}
		return result;
	}
}
