/*
 * service.cpp
 *
 *  Created on: Jul 26, 2013
 *      Author: vrobot
 */

#include "nodenet/service.h"

#include "nodenet/node.h"

#include <string.h>

namespace nodenet
{
	Service::Service()
	{

	}

	Service::~Service()
	{

	}

	int Service::getSources() const
	{
		return providers.size();
	}

	int Service::getSinks() const
	{
		return clients.size();
	}

	EntityType Service::getType() const
	{
		return EntityService;
	}

	int Service::pushRequest(int type, const DataBufferPtr & buffer, uint16_t id, const EndpointDesc & endpoint)
	{
		ServiceProviderBase * provider = providers.head;
		if(provider != NULL)
		{
			writeLog(2, "Service::pushRequest(requestId=%d)", id);
			provider->addEvent(buffer, ClientEventType::Request, id, endpoint);
			providers.rotate();
		}
		else
		{
			writeLog(2, "Service::pushRequest(requestId=%d) - no provider was found", id);
		}
		return 0;
	}

	int Service::sendResponse(DataBufferPtr buffer, int id, int flags)
	{
		RemoteEntityLink * link = getRemoteLinks();
		size_t count = getRemoteLinksCount();
		if(count == 0)
			writeLog(1, "Service::sendResponse(id=%d) - no valid endpoints to send", id);

		for(size_t i = 0; i < count; i++, link++)
		{
			Connection * connection = link->endpoint.connection;
			writeLog(2, "Service::sendResponse(id=%d) sending response to %s", id, link->endpoint.address.c_str());
			connection->sendUserData( buffer, id, 0, link->endpoint.address, flags);
			return 0;
		}

		return -1;
	}

	int Service::sendRequest(DataBufferPtr buffer, int id, int flags)
	{
		RemoteEntityLink * link = getRemoteLinks();

		size_t count = getRemoteLinksCount();
		if(count == 0)
			writeLog(1, "Service::sendRequest(id=%d) - no valid endpoints to send", id);

		for(size_t i = 0; i < getRemoteLinksCount(); i++, link++)
		{
			Connection * connection = link->endpoint.connection;
			writeLog(2, "Service::sendRequest(id=%d) sending request to %s", id, link->endpoint.address.c_str());
			connection->sendUserData( buffer, id, link->slotKey + SystemSlotsMax, link->endpoint.address, flags);
			return 0;
		}
		return -1;
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	class ServiceClientBase::Ticket : public RequestTicketBase
	{
	public:
		ServiceClientBase * client;

		~Ticket()
		{
			detach();
		}

		void detach()
		{
			if(client != NULL)
			{
				client->ticket = NULL;
				client = NULL;
			}
		}

		virtual int pushResponse(int type, DataBufferPtr buffer, const EndpointDesc & endpoint)
		{
			if(client != NULL)
			{
				client->addEvent(buffer, ClientEventType::Response, 0, endpoint);
			}
			return -1;
		}

		virtual void fillRequestInfo(MessageTypeInfo *  info)
		{
			if(client)
				memcpy(info, &client->typeReq, sizeof(MessageTypeInfo));
		}

		virtual void fillResponseInfo(MessageTypeInfo *  info)
		{
			if(client)
				memcpy(info, &client->typeRep, sizeof(MessageTypeInfo));

		}
	};

	///////////////////////////////////////////////////////////////////////////////
	ServiceClientBase::ServiceClientBase(const MessageTypeInfo & req, const MessageTypeInfo & rep)
	{
		alive = true;
		ticket = NULL;
		typeReq = req;
		typeRep = rep;
		currentEvent = NULL;
	}

	ServiceClientBase::~ServiceClientBase()
	{

	}

	void ServiceClientBase::attach(Node * node, const char * name)
	{
		detach();

		ServicePtr s = node->getService(name);
		if(s == service && this->node == node)	// if we referencing the same node and topic
			return;

		detach();

		this->node = node;
		this->service = s;

		service->clients.add(this);
	}

	void ServiceClientBase::detach()
	{
		if(node != NULL)
		{
			node = NULL;
			service = NULL;
		}
	}

	void ServiceClientBase::cancel()
	{
		if(ticket != NULL)
		{
			ticket->canceled = true;
			ticket->detach();
			ticket = NULL;
		}
	}

	void ServiceClientBase::waitProvider()
	{

	}

	EntityPtr ServiceClientBase::getEntity() const
	{
		return EntityPtr((Entity*)service.ptr());
	}

	bool ServiceClientBase::_sendRequest(DataBufferPtr buffer, int flags)
	{
		if(ticket != NULL)
			cancel();

		ticket = new Ticket();
		ticket->client = this;

		RequestID id = node->storeRequest(NULL, SystemRequestEntityList, ticket);

		service->sendRequest(buffer, id, flags);
		return true;
	}
	/////////////////////////////////////////////////////////////////////////////////
	ServiceProviderBase::ServiceProviderBase(const MessageTypeInfo & req, const MessageTypeInfo & rep)
	{
		typeReq = req;
		typeRep = rep;

		next = NULL;
		prev = NULL;

		currentEvent = NULL;
	}

	ServiceProviderBase::~ServiceProviderBase()
	{

	}

	void ServiceProviderBase::attach(Node * node, const char * name)
	{
		ServicePtr s = node->getService(name);
		if(s == service && this->node == node)	// if we referencing the same node and topic
			return;

		detach();

		service->lock();
		guard.lock();
		this->node = node;
		this->service = s;
		service->providers.add(this);
		guard.unlock();
		service->unlock();
	}

	void ServiceProviderBase::detach()
	{
		if(node != NULL)
		{
			service->lock();
			guard.lock();
			node = NULL;
			service->providers.erase(this);
			guard.unlock();
			service->lock();
		}
	}

	EntityPtr ServiceProviderBase::getEntity() const
	{
		return EntityPtr((Entity*)service.ptr());
	}

	int ServiceProviderBase::_sendResponse(DataBufferPtr data, int flags)
	{
		assert(currentEvent != NULL);
		int targetID = currentEvent->id;
		EndpointDesc endpoint = currentEvent->endpoint;
		freeEvent();
		return service->sendResponse(data, targetID, flags);
	}

	int ServiceProviderBase::raiseEvents()
	{
		return 0;
	}
}


