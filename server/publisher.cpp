/*
 * publisher.cpp
 *
 *  Created on: Nov 26, 2012
 *      Author: Dmitriy Kargin
 *  Implementation for Publisher class
 */

#include <nodenet/publisher.h>

namespace nodenet
{
	PublisherBase::PublisherBase(const MessageTypeInfo & type)
	{
		typeMsg = type;
		next = NULL;
		prev = NULL;
	}

	PublisherBase::~PublisherBase()
	{
		detach();
	}

	void PublisherBase::attach(Node * newNode, const char * name)
	{
		if(node != newNode)
		{
			detach();
			node = newNode;
			if(newNode != NULL)
			{
				topic = newNode->getTopic(name);
				node = newNode;
				node->attach(this, topic);
			}
		}
	}

	void PublisherBase::detach()
	{
		if(node != NULL)
		{
			topic->lock();
			topic->publishers.erase(this);
			topic->unlock();
			node->onClientsUpdated(topic);
			topic = NULL;
		}
	}

	EntityPtr PublisherBase::getEntity() const
	{
		return EntityPtr((Entity*)topic.ptr());
	}

	int PublisherBase::connections() const
	{
		if(node != NULL && topic != NULL)
			return topic->getRemoteSubs();
		return 0;
	}

	void PublisherBase::fillTypeInfo(MessageTypeInfo *  info)
	{
		*info = typeMsg;
	}
}


