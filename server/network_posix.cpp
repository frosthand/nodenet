#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>

#include <nodenet/transport_ip.h>

namespace nodenet {
namespace ip
{

const int InvalidSocket = -1;
const size_t DEFAULT_BUFLEN = 0xffff;

NetErrorType Socket::getError()
{
	switch(errno)
	{
	case EWOULDBLOCK: return WouldBlock;
	case EINTR: return SystemInterrupted;
	case EINVAL: return InvalidArgument;
	}
	return Unknown;
}

const char * Socket::getStrError()
{
	return strerror(errno);
}

int Socket::setBlocking(int value)
{
	assert(validSocket());

	int oldValue = fcntl(sockfd, F_GETFL);

	if (value && fcntl(sockfd, F_SETFL, oldValue & ~O_NONBLOCK) < 0) {
		return -1;
	} else if (!value && fcntl(sockfd, F_SETFL, oldValue | O_NONBLOCK) < 0) {
		return -1;
	}
	return 0;
}

int Socket::close()
{
	int result = ::close(sockfd);
	sockfd = InvalidSocket;
	return result;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}
}
