/*
 * node.cpp
 *
 *  Created on: Nov 26, 2012
 *      Author: Dmitriy Kargin
 */

#include <nodenet/publisher.h>
#include <nodenet/subscriber.h>
#include <nodenet/node.h>

#include <nodenet/topic.h>

#include <nodenet/navigator.h>

#include <stdarg.h>
#include <stdio.h>

#include "mastermsg/ResponseEntityList.hpp"
#include "mastermsg/RequestEntityList.hpp"

#include "errno.h"
#include <chrono>

namespace nodenet
{
	void copyTypeInfo(mastermsg::TypeInfo & target, const mastermsg::TypeInfo & source)
	{
		target.hash = source.hash;
		target.name = source.name;
	}

	void copyTypeInfo(mastermsg::TypeInfo & target, const MessageTypeInfo & source)
	{
		target.hash = source.hash;
		target.name = source.name;
	}

	void copyTypeInfo(MessageTypeInfo & target, const mastermsg::TypeInfo & source)
	{
		target.hash = source.hash;
		target.name = source.name;
	}

	void Node::writeLog(int level, const char * format, ...)
	{
		va_list	ap;
		va_start(ap,format);
		const int MAX_LEN=4096;
		static char buffer[MAX_LEN];
#ifdef _MSC_VER
			vsprintf_s(buffer, MAX_LEN,format,ap);
#else
			vsnprintf(buffer, MAX_LEN,format,ap);
#endif

		buffer[MAX_LEN-1]=0;
		printf("Node %s says: %s\n", this->getName(), buffer);
		va_end(ap);
	}
	/*
	 * 1. Connection established
	 * 2. Both send current published topic lists
	 */
	///////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////
	Node::Node(const char * name)
	{
		key = -1;
		nodeKey = -1;
		nodename = name;
		incomingEvents = 0;

		navigator = new Navigator();

		lastRequestID = 0;

		lastEntitySlot = 0;

		topicListener = NULL;
		/*
		activeTopicsCount = 0;
		for(size_t i = 0; i < MaxSlots; i++)
			activeTopicIndex[i] = i;*/
	}

	Node::~Node()
	{
		detachAllClients();
		detachAllTransports();
		deleteAllEntities();
		delete navigator;
	}

	const char * Node::getName() const
	{
		return nodename.c_str();
	}

	void Node::deleteAllEntities()
	{
		entitySlots.clear();
	}

	void Node::detachAllClients()
	{

    }

	void Node::detachAllTransports()
	{
		Transports ts = getTransports();
		for(Transports::iterator it = ts.begin(); it != ts.end(); ++it)
		{
			Transport * transport = *it;
			transport->detach();
		}
	}

	void Node::detachedTransport(Transport * transport)
	{
		transports.erase(transport);
	}

	RequestID Node::storeRequest(Connection * connection, int command, RequestTicketBase * request)
	{
		RequestID id = ++lastRequestID;
		requests[id] = request->addref();
		return id;
	}

	struct NodeHandlers
	{
		static void s_onResoponseEntityList(const mastermsg::RequestEntityList& request, const mastermsg::ResponseEntityList & response, void * userdata, const EndpointDesc & endpoint)
		{
			Node * node = (Node*)userdata;
			node->writeLog(0, "s_onResponseEntityList(...)");
			Navigator * navigator = node->getNavigator();
			Navigator::NodeInfo * info = navigator->getNodeInfo(response.nodeName.c_str());

			for(int i = 0; i < response.servicesCount; i++)
			{
				// Fill navigator data
				mastermsg::EntityInfo entity = response.services[i];
				EntityInfo & ei = info->getEntityInfo(entity.name.c_str());
				ei.name = entity.name;
				//ei.type = entity.type;
				copyTypeInfo(ei.dataReq, entity.req);
				copyTypeInfo(ei.dataRep, entity.rep);
				copyTypeInfo(ei.dataProcess, entity.proc);
				ei.sources = entity.sources;
				ei.sinks = entity.sinks;
				ei.type = entity.type;
				ei.slot = entity.slot;
				// Generate topic links
				Entity * en = node->findEntity(ei.name.c_str());
				if(en != NULL)
				{
					RemoteEntityLink link;
					link.endpoint = endpoint;
					link.remoteSlot = ei.slot;
					link.pubs = ei.sources;
					link.subs = ei.sinks;
					en->addLink(link);
				}
			}
		}
	};

	void Node::onUserConnectionClosed(Transport * transport, Connection * connection)
	{
		int i = 0;
		for(EntitySlots::iterator it = entitySlots.begin(); it != entitySlots.end(); ++it, i++)
		{
			EntityPtr ptr = it->second;
			ptr->onLinkClosed(ConnectionPtr(connection));
		}
	}

	void Node::onUserConnectionCreated(Transport * transport, Connection * connection)
	{
		RequestTicketSystem<mastermsg::RequestEntityList, mastermsg::ResponseEntityList> * ticket = new RequestTicketSystem<mastermsg::RequestEntityList, mastermsg::ResponseEntityList>();

		ticket->request.filter="*";
		ticket->userdata = this;
		ticket->handler = &NodeHandlers::s_onResoponseEntityList;

		RequestID id = storeRequest(connection, SystemRequestEntityList, ticket);

		int size = ticket->request.getEncodedSize();
		DataBufferPtr buffer = writeBuffer(ticket->request);
		connection->sendUserData(buffer, id, SystemRequestEntityList);
	}

	EntityPtr Node::getEntity(uint16_t slot)
	{
		if(slot >= MaxSlots)
			return EntityPtr();
		EntityPtr result = entitySlots[slot];// + slot;
		return result;
	}

	int Node::processResponse(const DataBufferPtr & buffer, uint16_t id, const EndpointDesc & endpoint)
	{
		writeLog(0, "Node::processResponse(%d)", id);
		RequestStorage::iterator it = requests.find(id);
		if( it== requests.end())
			return -1;
		RequestTicketBase * ticket = it->second;

		ticket->pushResponse(0, buffer, endpoint);

		requests.erase(it);
		delete ticket;
		return -1;
	}

	int Node::processUserMessage(const DataBufferPtr & buffer, int slot)
	{
		std::lock_guard<std::mutex> lock(entityGuard);

		EntityPtr entity = getEntity(slot);
		// Check if slot is valid
		if(entity == NULL || entity->getType() != EntityTopic)
		{
			writeLog(0, "Node::processUserMessage(%d) - invalid topic in slot", slot);
			return -1;
		}

		TopicPtr topic((Topic*)entity.ptr());
		topic->pushMessage(0, buffer);
		incomingEvents++;
		entityCondition.notify_one();
		return 0;
	}

	int Node::processSystemMessage(const DataBufferPtr & buffer, int slot)
	{
		writeLog(0, "Node::processSystemMessage(%d) - no system messages implemented", slot);
		return -1;
	}

	int Node::processUserRequest(const DataBufferPtr & buffer, uint16_t id, uint16_t slot, const EndpointDesc & endpoint)
	{
		writeLog(0, "Node::processUserRequest(%d) - got request", slot);
		std::lock_guard<std::mutex> lock(entityGuard);
		EntityPtr entity = getEntity(slot);
		// Check if slot is valid
		if(entity == NULL || entity->getType() != EntityService)
		{
			writeLog(0, "Node::processUserMessage(%d) - invalid topic in slot", slot);
			return -1;
		}

		ServicePtr service((Service*)entity.ptr());
		service->pushRequest(0, buffer, id, endpoint);
		incomingEvents++;
		entityCondition.notify_one();
		writeLog(0, "Node::processUserRequest(%d) - done", slot);
		return -1;
	}

	void Node::respondGreeting(const DataBufferPtr & buffer, int id, const EndpointDesc & endpoint)
	{

	}

	void Node::respondEntityList(const DataBufferPtr & buffer, int id, const EndpointDesc & endpoint)
	{
		writeLog(0, "Node::responseEntityList(%d)", id);
		mastermsg::ResponseEntityList response;
		response.nodeName = this->getName();
		response.servicesCount = entitySlots.size();
		response.services.resize(entitySlots.size());

		int i = 0;
		for(EntitySlots::iterator it = entitySlots.begin(); it != entitySlots.end(); ++it, i++)
		{
			mastermsg::EntityInfo & info = response.services[i];
			Entity * topic = it->second;
			info.name = topic->getName();
			info.slot = it->first;
			info.type = topic->getType();
			info.sinks = topic->getSinks();
			info.sources = topic->getSources();
			copyTypeInfo(info.req,topic->dataReq);
			copyTypeInfo(info.rep,topic->dataRep);
			copyTypeInfo(info.proc,topic->dataProcess);
		}

		if(endpoint.connection)
			sendUserData(endpoint, id, 0, writeBuffer(response));
	}

	void Node::sendUserData(const EndpointDesc & endpoint, int id, int slot, DataBufferPtr buffer)
	{
		Connection * c = (Connection*)endpoint.connection.ptr();
		c->sendUserData(buffer, id, 0, endpoint.address);
	}

	void Node::respondEntityStatus(const DataBufferPtr & buffer, int id, const EndpointDesc & endpoint)
	{

	}

	void Node::respondPing(const DataBufferPtr & buffer, int id, const EndpointDesc & endpoint)
	{

	}

	int Node::processSystemRequest(const DataBufferPtr & buffer, uint16_t id, uint16_t slot, const EndpointDesc & endpoint)
	{
		switch(slot)
		{
		case SystemResponse:
			return processResponse(buffer, id, endpoint);
		case SystemRequestGreeting:
			respondGreeting(buffer, id, endpoint);
			break;
		case SystemRequestEntityList:
			respondEntityList(buffer, id, endpoint);
			break;
		case SystemRequestEntityStatus:
			respondEntityStatus(buffer, id, endpoint);
			break;
		case SystemRequestPing:
			respondPing(buffer, id, endpoint);
		default:
			break;
		}
		return -1;
	}

	int Node::onDataTicketComplete(const DataBufferPtr & buffer, uint16_t id, uint16_t slot, const EndpointDesc & endpoint)
	{
		if(id == 0 && slot >= SystemSlotsMax)			// user level message
		{
			return processUserMessage(buffer, slot - SystemSlotsMax);
		}
		else if(id == 0 && slot < SystemSlotsMax)		// system level message
		{
			return processSystemMessage(buffer, slot);
		}
		else if(id > 0 && slot >= SystemSlotsMax) 		// user level request
		{
			return processUserRequest(buffer, id, slot - SystemSlotsMax, endpoint);
		}
		else if(id > 0 && slot < SystemSlotsMax) 		// system level request
		{
			return processSystemRequest(buffer, id, slot,  endpoint);
		}
		else
		{
			this->writeLog(0, "Node::onDataTicketComplete(size=%d,id=%d,slot=%d) - strange packet header", buffer.size(), id, slot);
		}
		return -1;
	}

	int Node::raiseEvents(int timeoutMS)
	{
		// Topic can be created
		// Topic can be removed
		// entity access should be as small as possible
		int result = 0;
		if(incomingEvents == 0)
		{
			std::unique_lock<std::mutex> lock(entityGuard);
			std::cv_status result = entityCondition.wait_for(lock, std::chrono::milliseconds(timeoutMS));
			if(result == std::cv_status::timeout)
				return -1;
		}
		else
		{
			// fill in entity cache
			std::vector<EntityPtr> cached;
			cached.reserve(entitySlots.size());
			for(EntitySlots::iterator it = entitySlots.begin(); it != entitySlots.end(); ++it)
				cached.push_back(it->second);
			incomingEvents = 0;
			entityGuard.unlock();

			// process all events
			for(int i = 0; i < cached.size(); i++)
				result += cached[i]->raiseEvents();
		}

		return result;
	}

	Entity * Node::findEntity(const char * name)
	{
		/// find existing topic
		for(EntitySlots::iterator it = entitySlots.begin(); it != entitySlots.end(); ++it)
		{
			Entity * entity = it->second;

			if(strcmp(entity->getName(), name) == 0)
			{
				return entity;
			}
		}
		return NULL;
	}

	Topic * Node::findTopic(const char * topicName)
	{
		/// find existing topic
		for(EntitySlots::iterator it = entitySlots.begin(); it != entitySlots.end(); ++it)
		{
			Entity * entity = it->second;
			if(entity->getType() == EntityTopic)
			{
				Topic * topic = (Topic*)entity;
				if(strcmp(topic->getName(), topicName) == 0)
				{
					return topic;
				}
			}
		}
		return NULL;
	}

	Service * Node::findService(const char * topicName)
	{
		/// find existing topic
		for(EntitySlots::iterator it = entitySlots.begin(); it != entitySlots.end(); ++it)
		{
			Entity * entity = it->second;
			if(entity->getType() == EntityTopic)
			{
				Service * service = (Service*)entity;
				if(strcmp(service->getName(), topicName) == 0)
				{
					return service;
				}
			}
		}
		return NULL;
	}

	TopicPtr Node::getTopic(const char * topicName)
	{
		TopicPtr result(findTopic(topicName));

		if(result.ptr() == NULL)
		{
			writeLog(0, "Node::getTopic(%s) - failed to found existing, creating new", topicName);
			entityGuard.lock();
			result = TopicPtr(new Topic());
			size_t newIndex = lastEntitySlot++;
			result->slot = newIndex;
			entitySlots[newIndex] = EntityPtr(result);
			entityGuard.unlock();
		}
		return result;
	}


	ServicePtr Node::getService(const char * topicName)
	{
		ServicePtr result(findService(topicName));

		if(result.ptr() == NULL)
		{
			writeLog(0, "Node::getService(%s) - failed to found existing, creating new", topicName);
			entityGuard.lock();
			result = ServicePtr(new Service());
			size_t newIndex = lastEntitySlot++;
			result->slot = newIndex;
			entitySlots[newIndex] = EntityPtr(result);
			entityGuard.unlock();
		}
		return result;
	}

	size_t Node::getTopicsMax() const
	{
		return MaxSlots;
	}

	size_t Node::getTopicsCount() const
	{
		return entitySlots.size();
	}

	/// Publisher or Subscriber called this handler
	void Node::onClientsUpdated(Topic * topic)
	{
		const char * name = topic->getName();
		/// TODO: implement
		/// 1. Get all known links
		/// 2. Send notification
/*
		DataTopicEvent data;
		data.subs = topic->subscribers.size();
		data.pubs = topic->publishers.size();
		data.nodeKey = this->nodeKey;
		strncpy(data.nodeName, getName(), sizeof(data.nodeName));
		strncpy(data.topicName, topic->getName(), sizeof(data.topicName));*/

		//sendMaster(MessageTopicState(data));
	}
	/////////////////////////////////////////////////////////////////////////////////////
	void Node::attach(PublisherBase * object, Topic * topic)
	{
		//assert(this == object->node);
		MessageTypeInfo info;
		object->fillTypeInfo(&info);

		topic->lock();

		if(topic->dataProcess.hash == 0)
			topic->dataProcess = info;
		/// TODO: implement
		topic->unlock();

		topic->publishers.add(object);
		onClientsUpdated(topic);
	}

	void Node::attach(SubscriberBase * object, Topic * topic)
	{
		//assert(this == object->node);
		MessageTypeInfo info;
		object->fillTypeInfo(&info);
		/// TODO: implement
		topic->subscribers.add(object);
		onClientsUpdated(topic);
	}

	Node::Transports Node::getTransports() const
	{
		return transports;
	}
}

