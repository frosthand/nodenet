/*
 * subscriber.cpp
 *
 *  Created on: Nov 26, 2012
 *      Author: vrobot
 */

#include <nodenet/node.h>
#include <nodenet/subscriber.h>

namespace nodenet
{
	SubscriberBase::SubscriberBase(const MessageTypeInfo & type)
	{
		typeMsg = type;
		node = NULL;
		topic = NULL;

		prev = NULL;
		next = NULL;
	}

	SubscriberBase::~SubscriberBase()
	{
		detach();
	}

	void SubscriberBase::attach(Node * newNode, const char * name)
	{
		if(node != newNode)
		{
			detach();
			node = newNode;
			if(newNode != NULL)
			{
				topic = newNode->getTopic(name);
				node = newNode;
				node->attach(this, topic);
			}
		}
	}

	void SubscriberBase::detach()
	{
		if(node != NULL)
		{
			topic->lock();
			topic->subscribers.erase(this);
			topic->unlock();
			node->onClientsUpdated(topic);
			topic = NULL;
			node = NULL;
		}
	}

	EntityPtr SubscriberBase::getEntity() const
	{
		return EntityPtr((Entity*)topic.ptr());
	}

	void SubscriberBase::fillTypeInfo(MessageTypeInfo *  info)
	{
		*info = typeMsg;
	}
}
