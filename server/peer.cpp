//#include "stdafx.h"
#include <nodenet/peer.h>

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdarg.h>
#include <stdio.h>

#ifdef FROSTTOOLS_USE_POLL
#include <poll.h>
#endif

//#include <select.h>

#if defined(WIN32) || defined(__WIN32__)
#else
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#endif

namespace nodenet {
namespace ip{
	const int InvalidSocket=-1;
	/// Wraps some generic socket methods
	/// For winsock2 inits socket library
	/// Contains some error handling
	/// TODO: move all log related code to network from Peer

	const size_t DEFAULT_BUFLEN = 0xffff;

	/// Wrapper for network address

	PeerAddress::PeerAddress()
	{
		type = -1;	/// Invalid?
		from.sin_family = AF_INET;
		from.sin_addr.s_addr = htonl(INADDR_ANY );
		from.sin_port = -1;
	}

	PeerAddress::PeerAddress(const PeerAddress &pa)
	{
		memcpy(&from, &pa.from, sizeof(sockaddr_in));
		type = pa.type;
	}

	PeerAddress & PeerAddress::operator=(const PeerAddress & pa)
	{
		memcpy(&from, &pa.from, sizeof(sockaddr_in));
		type = pa.type;
		return *this;
	}

	void PeerAddress::setHostname(const char * name)
	{
		type = 0;
		from.sin_addr.s_addr = inet_addr(name);
	}

	void PeerAddress::setPort(int port)
	{
		from.sin_port = htons(port);
	}

	const char * PeerAddress::getHostname() const
	{
		return inet_ntoa(from.sin_addr);
	}

	int PeerAddress::getPort() const
	{
		return ntohs(from.sin_port);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	Socket::Socket()
	{
		socketType = SocketTCP;
		slave = false;
		slotId = -1;
		state = Empty;
		buffer = NULL;
		bufferLength = 0;
		bytesRecieved = 0;
		bytesTotal = 0;
		sockfd = InvalidSocket;
		events = 0;
		//manualRead = false;
		userdata = NULL;
	}

	Socket::~Socket()
	{
		releaseBuffer();
	}

	bool Socket::validSocket() const
	{
		return sockfd != InvalidSocket;
	}

	bool Socket::create(SocketType socketType)
	{
		assert(state == Socket::Empty);
		assert(validSocket() == false);
	//LogFunction(*log);
		sockfd = InvalidSocket;
		char flag = 0;
		int ret = 0;
		switch (socketType)
		{
		case SocketTCP:
			sockfd = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
			break;
		case SocketUDP:
			sockfd = ::socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
			break;
		}

		return validSocket();
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// create listening socket for incoming connection
	bool Socket::listen(int port)
	{
		assert(state == Socket::Empty);
		if(create(Socket::SocketTCP))
		{
	#ifdef _WIN32
	#else
			int reuse_addr = 1;
			setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &reuse_addr,
					sizeof(reuse_addr));
	#endif
			//s.userdata = userdata;
			int flagNodelay = 1;
			if (setTCPNoDelay(flagNodelay) == -1)
			{
				writeLog(0, "Peer::listen() cannot set nodelay=%d", flagNodelay);
			}

			int flagNonblock = 0;
			if (setBlocking(flagNonblock) == -1)
			{
				writeLog(0, "Peer::listen() cannot set blocking=%d", flagNonblock);
			}
			// 2. bind socket
			//writeLog(0, "binding a socket");
			sockaddr_in addr; //
			addr.sin_family = AF_INET;      //

			addr.sin_addr.s_addr = htonl(INADDR_ANY );   //
			addr.sin_port = htons(port);   //

			if (bind(sockfd, (sockaddr*) &addr, sizeof(addr)) < 0) {
				writeLog(0, "Peer::listen() bind failed: %s", getError());
				close();
				return false;
			} else {
				socklen_t len;
				struct sockaddr_in foo;

				// 3. put it to listening mode
				if (::listen(sockfd, maxClients) < 0) {
					//ReportError(network.getLastError(), "Server listening failed");
					close();
					return false;
				}

				len = sizeof(foo);
				getsockname(sockfd, (struct sockaddr *) &foo, &len);
				writeLog(0, "listening %d binded to %s:%d", port, inet_ntoa(foo.sin_addr),ntohs(foo.sin_port));
			}
		}
		size_t result = -1;
		// 1. try to init listening socket
		if (sockfd == InvalidSocket) {
			writeLog(0, "-socket failed: %s", getError());
			//network.closeSocket(s.socket);
			return -1;
		}

		state = Socket::AcceptingTCP;

		return true;
	}

	bool Socket::connect( const PeerAddress * address, size_t flags)
	{
		assert(state == Socket::Empty);
		if ((sockfd = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP))
				== InvalidSocket)
		{
			//callError(SocketCreation);
			//network.getLog()->line(0, "error at socket(): %ld",	network.getLastError());
			return false;
		}

		// no delay for TCP
		int flagNodelay = 1;
		if (setTCPNoDelay(flagNodelay) == -1)
		{
			writeLog(0, "Peer::connect() cannot set nodelay=%d", flagNodelay);
		}

		state = Socket::StartConnectingTCP;
		memcpy(&target_addr, &address->from, sizeof(target_addr));

		/*size_t slotId = addSocket(socket);
		if(slotId == -1)
			return -1;*/

		//socket.userdata = userdata;

		bool wait = (flags & ConnectSync) != 0;
		if(wait)
		{
			state = Socket::ConnectingTCP;
			processTCPConnecting();

			if(state != Socket::WorkingTCP)
			{
				/// failed to connect?
				return false;
			}
			int flagBlock = 0;
			if (setBlocking(flagBlock) == -1)
			{
				writeLog(0, "Peer::connect() cannot set blocking=%d", flagBlock);
			}
			return true;
		}

		return true;
	}

	int Socket::setTCPNoDelay(int value)
	{
		int flag = value;
		return setsockopt(sockfd, IPPROTO_TCP, TCP_NODELAY, (char*) &flag,sizeof(flag));
	}

	int Socket::send(const void * data, size_t size, PeerAddress * address) {
		if (state != Socket::WorkingTCP)
			return -1;
		int sendFlags = 0;
	#if defined(WIN32) || defined(__WIN32__)
	#else
		sendFlags |= MSG_NOSIGNAL;
	#endif

		int bytesData = 0;
		if (size > 0) {
			bytesData = ::send(sockfd, (const char*) data, size, sendFlags);
			if (bytesData <= 0) {
				/// TODO: check for EAGAIN/EWOULDBLOCK
				NetErrorType err = Socket::getError();
				if(err != WouldBlock)
				{
					state = Socket::Dying;
					return -1;
				}
				else
				{
					//printf("Message would block\n");
					return 0;
				}
			}
		}
		return bytesData;
	}

	PeerAddress Socket::getRemoteAddress() const
	{
		PeerAddress result;
		socklen_t len = sizeof(result.from);
		if(getpeername(sockfd, (sockaddr*)&result.from, &len) != 0 || len > sizeof(result.from) || len == 0)
		{
			writeLog(0, "Socket::getSlotAddress() error: %s", strerror(errno));
		}
		else
			result.type = 0;
		return result;
	}

	PeerAddress Socket::getLocalAddress() const
	{
		PeerAddress result;

		socklen_t len = sizeof(result.from);
		if(getsockname(sockfd, (sockaddr*)&result.from, &len) < 0 || len > sizeof(result.from))
		{
			writeLog(0, "Peer::getSlotAddress() error: %s", strerror(errno));
		}
		else
			result.type = 0;

		return result;
	}

	void Socket::releaseBuffer() {
		free(buffer);
		buffer = NULL;
	}

	void Socket::resizeBuffer(size_t newLength) {
		buffer = (unsigned char*) realloc(buffer, newLength);
		bufferLength = buffer ? newLength : 0;
	}

	bool Socket::doAccept(Socket & incoming) {
		assert(state == Socket::AcceptingTCP);
		// Accept a client socket
		socklen_t addrLen = sizeof(target_addr);

		incoming.sockfd = accept(sockfd, (sockaddr*) &target_addr, &addrLen);
		if (!incoming.validSocket())
		{
			if (Socket::getError() == WouldBlock) {
				writeLog(0, "Peer::processAccepting: would block");
			} else
				writeLog(0, "Peer::processAccepting: failed to accept");
			return false;
		}
		//writeLog(0, " accepted connection");
		incoming.state = Socket::WorkingTCP;
		incoming.slave = true;
		incoming.socketType = Socket::SocketTCP;
		incoming.setBlocking(0);

		return true;
	}

	///////////////////////////////////////////////////////////////////////////
	// try to connect
	bool Socket::processTCPConnecting() {
		assert(state == Socket::ConnectingTCP || state == Socket::StartConnectingTCP);
		int res = ::connect(sockfd, (sockaddr*) &target_addr, sizeof(target_addr));
		if (res < 0)
		{
			NetErrorType err = getError();
			if(err == WouldBlock)
				state = Socket::ConnectingTCP;	/// try to connect next time
			else
			{
				state = Socket::Dying;
				writeLog(0,	"Peer::processConnecting() failed to connect, errno=%d, err=%s",
						errno, strerror(errno));
			}
		} else {
			state = Socket::WorkingTCP;
			setBlocking(0);
			//writeLog(0, "connected");
			// no delay for TCP
			int flag = 1;
			int ret = setsockopt(sockfd, IPPROTO_TCP, TCP_NODELAY, (char*) &flag,	sizeof(flag));
			if (ret == -1) {
				writeLog(0,	"Peer::processConnecting() cannot turn on nodelay, errno=%d, err=%s",
						errno, strerror(errno));
			}
			return true;
		}
		return false;
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	Peer::Peer(int bufferSize)
	{
		socketsAllocated = 0;
		socketsUsed = 0;
		listener = NULL;
		dying = false;
		lockedSocketsArray = false;
		sockets = NULL;

		sockets = (Socket*) realloc(sockets, maxClients * sizeof(Socket));
		if(sockets)
		{
			Socket zeroSocket;
			socketsAllocated = maxClients;
			for (size_t i = 0; i < socketsAllocated; i++)
			{
				memcpy(sockets+i, &zeroSocket, sizeof(Socket));
			}
		}
#ifdef FROSTTOOLS_USE_POLL

#ifdef FROSTTOOLS_USE_POLL
		polls =  NULL;
		polls = (pollfd*) realloc(polls, maxClients * sizeof(pollfd));

		if (polls == NULL)
		{
			socketsAllocated = 0;
			//callError(Memory);
		}
#endif
#endif
	}

	Peer::~Peer() {
		closeAll();
	}

	void Peer::closeAll()
	{
		if(sockets != NULL)
		{
			for (size_t i = 0; i < socketsAllocated; i++)
			{
				sockets[i].close();
				sockets[i].releaseBuffer();
			}
			free(sockets);
			sockets = NULL;
		}
#ifdef FROSTTOOLS_USE_POLL
		if(polls != NULL)
		{
			free(polls);
			polls = NULL;
		}
#endif
	}

	PeerAddress Peer::getLocalAddress(SlotHandle id) const
	{
		PeerAddress result;
		if(checkSlotHandle(id) >= 0)
			return getSlot(id).getLocalAddress();
		else
		{
			writeLog(0, "Peer::getSlotAddress() invalid slot");
		}
		return result;
	}

	PeerAddress Peer::getRemoteAddress(SlotHandle id) const
	{
		PeerAddress result;
		if(checkSlotHandle(id) >= 0)
			return getSlot(id).getRemoteAddress();
		else
		{
			writeLog(0, "Peer::getSlotAddress() invalid slot");
		}
		return result;
	}

	const Socket & Peer::getSlot(SlotHandle handle) const
	{
		return sockets[handle];
	}

	Socket & Peer::getSlot(SlotHandle handle)
	{
		return sockets[handle];
	}

	int Peer::close(SlotHandle handle)
	{
		//Lock _lock(*this);
		if(checkSlotHandle(handle) < 0)
			return -1;
		Socket & s = getSlot(handle);
		s.close();
		s.releaseBuffer();
		s.state = Socket::Empty;
		return 0;
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	int Peer::send(SlotHandle handle, const void * data, size_t size, PeerAddress * address) {
		//Lock _lock(*this);
		if(checkSlotHandle(handle)<0)
			return -1;
		Socket & s = getSlot(handle);
		return s.send(data, size, address);
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	size_t Peer::addSocket(const Socket & s) {
		//assert(lockedSocketsArray == false);
		/*if(lockedSocketsArray)
		{
			callError(SlotsArrayLocked);
			return -1;
		}*/
		/// search for the first empty socket
		for (size_t i = 0; i < socketsAllocated; i++) {
			if (sockets[i].state == Socket::Empty) {
				memcpy(sockets + i, &s, sizeof(Socket));
				return i;
			}
		}

		/// or reallocate socket array for new slot
		size_t newSize = socketsAllocated + 1;

		sockets = (Socket*) realloc(sockets, newSize * sizeof(Socket));

#ifdef FROSTTOOLS_USE_POLL
		polls = (pollfd*) realloc(polls, newSize * sizeof(pollfd));

		if (polls == NULL)
		{
			socketsAllocated = 0;
			callError(Memory);
			return -1;
		}
#endif
		/// cannot realloc - memory error
		if (sockets == NULL) {
			socketsAllocated = 0;
			callError(Memory);
			return -1;
		}

		memcpy(sockets + socketsAllocated, &s, sizeof(Socket));
		socketsAllocated = newSize;
		return socketsAllocated - 1;
	}

	void Peer::setNoDelay(size_t slot, bool value) {
		if(checkSlotHandle(slot) < 0)
			return;
		int flag = value ? 1 : 0;
		int ret = sockets[slot].setTCPNoDelay(value);
		writeLog(0, "Peer::setNoDelay cannot turn on nodelay, errno=%d, err=%s",
							errno, strerror(errno));
		/*
		int ret = setsockopt(sockets[slot].getfd(), IPPROTO_TCP, TCP_NODELAY,
				(char*) &flag, sizeof(flag));
		if (ret == -1) {
			writeLog(0, "Peer::setNoDelay cannot turn on nodelay, errno=%d, err=%s",
					errno, strerror(errno));
		}*/
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	size_t Peer::getClientsUsed() const {
		//Lock _lock(*(Peer*)this);
		return socketsAllocated;
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	Peer::SlotHandle Peer::connect( const PeerAddress * address, bool wait, void * userdata)
	{
		Socket socket;

		if(socket.connect(address, wait ? Socket::ConnectSync : Socket::ConnectAsync))
		{
			size_t slotId = addSocket(socket);
			if(slotId == -1)
				return -1;
			return makeHandle(slotId);
		}
		return -1;
	}
#ifdef USE_DEPRECATED
	Peer::SlotHandle Peer::createUDP(int port, void * userdata)
	{
		Socket s;
		if (!s.create(Socket::SocketUDP))
		{
			writeLog(0, "Peer::createUDP(%d) - socket() failed: %s", port, s.getError());
			return -1;
		}
#ifdef _WIN32
#else
		int reuse_addr = 1;
		setsockopt(s.sockfd, SOL_SOCKET, SO_REUSEADDR, &reuse_addr,
				sizeof(reuse_addr));
#endif
		s.userdata = userdata;
		int flagNonblock = 0;
		if (s.setBlocking(flagNonblock) == -1)
		{
			writeLog(0, "Peer::createUDP() cannot set blocking=%d", flagNonblock);
		}
		// 2. bind socket
		sockaddr_in addr; //
		addr.sin_family = AF_INET;      //
		addr.sin_port = htons(port);   	//
		addr.sin_addr.s_addr = htonl(INADDR_ANY );   //
		if (bind(s.sockfd, (sockaddr*) &addr, sizeof(addr)) < 0) {
			writeLog(0, "Peer::createUDP() bind failed: %s", s.getError());
			s.close();
			return -1;
		}
		s.state = Socket::WorkingUDP;

		//Lock _lock(*this);
		size_t result = addSocket(s);
		return result >=0 ? makeHandle(result): -1;
	}
#endif
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// create listening socket for incoming connection
	Peer::SlotHandle Peer::listen(int port, void * userdata)
	{
		Socket s;
		if(s.listen(port))
		{
			Peer::SlotHandle result = addSocket(s);
			return makeHandle(result);
		}
		return -1;
	}

	Peer::SlotHandle Peer::makeHandle(size_t slot) const
	{
		return slot;
	}

	void Peer::setSlotUserdata(SlotHandle handle, void * userdata)
	{
		if(checkSlotHandle(handle) < 0)
			return;

		getSlot(handle).userdata = userdata;
	}

	void * Peer::getSlotUserdata(SlotHandle handle) const
	{
		if(checkSlotHandle(handle) < 0)
			return NULL;
		return getSlot(handle).userdata;
	}

	///////////////////////////////////////////////////////////////////////////
	// try to recieve incoming connection
	bool Peer::processTCPAccepting(size_t i) {
		Socket & s = sockets[i];
		// Accept a client socket
		Socket incoming;
		if(s.doAccept(incoming))
		{
			size_t newSocketID = addSocket(incoming);

			if (newSocketID >= 0) {
				if (!listener
						|| !listener->onIncomingConnection(this, i, newSocketID)) {
					sockets[newSocketID].state = Socket::Dying;
				}
				return sockets[newSocketID].state != Socket::Dying;
			}
		}
		return false;
	}
	///////////////////////////////////////////////////////////////////////////
	// try to connect
	void Peer::processTCPConnecting(size_t i) {
		//LogFunction(*network.getLog());
		Socket & s = sockets[i];
		if(s.processTCPConnecting() && listener)
			listener->onSlotConnected(this, i);
	}

	bool Socket::doReceive(PeerAddress &address)
	{
		socklen_t from_size = sizeof(address.from);
		if (bufferLength == 0) {
			resizeBuffer(DEFAULT_BUFLEN);
		}

		if (bufferLength >= bytesRecieved) {
			int bytes = ::recvfrom(sockfd, (char*) buffer + bytesRecieved,
					bufferLength - bytesRecieved, 0, (sockaddr*)&address.from, &from_size);
			if (bytes <= 0)
			{
				NetErrorType err = getError();
				if(err == WouldBlock)
				{
				}
				else
				{
					writeLog(0, "Socket::processRecieving() - socket was closed");
					state = Socket::Dying;
				}
				return false;
			}
			bytesRecieved += bytes;
			bytesTotal += bytes;
		} else {
			writeLog(1, "Socket receive error: %s", getErrorString(BufferOverrun));

			state = Socket::Dying;
			return false;
		}
		return true;
	}
	///////////////////////////////////////////////////////////////////////////
	// try to receive incoming message
	void Peer::processReceiving(size_t i)
	{
		Socket & s = sockets[i];
		PeerAddress address;
		if(!s.doReceive(address))
			return;

		if (s.bytesRecieved > 0 && listener)
		{			
			unsigned int processed = listener->onRecieve(this, i, s.buffer, s.bytesRecieved, &address);
			
			assert(processed <= s.bytesRecieved && processed >= 0);
			assert(s.bytesRecieved >= 0);
			int unreceivedData = s.bytesRecieved - processed;
			if(unreceivedData > 0)
				memmove(s.buffer, s.buffer + processed, unreceivedData);
			assert(unreceivedData >= 0);
			s.bytesRecieved = unreceivedData;
		}
	}

#ifdef FROSTTOOLS_USE_POLL
	void Peer::checkSlots_poll(timeval &timeout)
	{
		unsigned int selectAwaits = 0, selected = 0;
		// 1. Fill socket sets
		for(size_t i = 0; i < socketsAllocated; i++)
		{
			Socket & s = sockets[i];
			polls[i].events = 0;

			if(!s.validSocket())
			{
				polls[i].fd = 0;
				continue;
			}
			polls[i].fd = s.getfd();

			if(s.shouldEventIn())
				polls[i].events |= POLLIN;
			if(s.shouldEventOut())
				polls[i].events |= POLLOUT;
			if(s.shouldEventError())
				polls[i].events |= POLLERR;
			selectAwaits++;
		}

		if (selectAwaits > 0)
		{
			unsigned int msec = timeout.tv_usec/1000;
			if(msec == 0)
				msec = 1;
			selected = poll(polls, socketsAllocated, msec);
		}

		if (selected < 0) {
			writeLog(0, "Poll error: %s", Socket::getError());
		}

		/// gather events
		if(selected > 0)
		{
			for( size_t i = 0; i < socketsAllocated; ++i )
			{
				Socket &s = sockets[i];
				s.events = 0;

				if(polls[i].fd == 0)
					continue;

				if(polls[i].revents & POLLIN)
					s.events |= Socket::EventIn;
				if(polls[i].revents & POLLOUT)
					s.events |= Socket::EventOut;
				if(polls[i].revents & POLLERR)
					s.events |= Socket::EventError;
			}
		}
	}
#else
	void Peer::checkSlots_select(timeval &timeout)
	{
		FD_ZERO(&readfds);
		FD_ZERO(&writefds);
		FD_ZERO(&exceptfds);
		int selectAwaits = 0;
		int selected = 0;
		int maxfds = 0;
		// 1. Fill socket sets
		for( size_t i = 0; i < socketsAllocated; ++i )
		{
			Socket & s = sockets[i];

			if(!s.validSocket())
				continue;

			if(s.sockfd > maxfds)
				maxfds = s.sockfd;

			if(s.shouldEventIn())
			{
				FD_SET(s.sockfd, &readfds);
			}

			if(s.shouldEventOut())
			{
				FD_SET(s.sockfd, &writefds);
			}

			if(s.shouldEventError())
			{
				FD_SET(s.sockfd, &exceptfds);
			}

			selectAwaits++;
		}

		if( selectAwaits > 0 )
		{
			do
			{
				selected = select(maxfds+1, &readfds, &writefds, &exceptfds, &timeout);
			}while(selected < 0 && Socket::getError() == SystemInterrupted );
		}

		if( selected < 0 )
		{
			writeLog(0,"Select error %s", getErrorString(Socket::getError()));
		}
		/// gather events
		if(selected > 0)
		{
			for( size_t i = 0; i < socketsAllocated; ++i )
			{
				Socket &s = sockets[i];
				s.events = 0;
				if(FD_ISSET(s.sockfd, &readfds))
				{
					s.events |= Socket::EventIn;
				}
				if(FD_ISSET(s.sockfd, &writefds))
				{
					s.events |= Socket::EventOut;
				}
				if(FD_ISSET(s.sockfd, &exceptfds))
				{
					s.events |= Socket::EventError;
				}
			}
		}
	}
#endif
	/// internal call
	void Peer::updateSlotState(size_t slotId)
	{
		Socket & s = sockets[slotId];
		//int16_t events = getSlotEvents(slotId);
		if(s.state == Socket::StartConnectingTCP)
			processTCPConnecting(slotId);
		else if (s.hasEvent(Socket::EventOut) && s.state == Socket::ConnectingTCP)
			processTCPConnecting(slotId);
		else if (s.hasEvent(Socket::EventIn))
		{
			if (s.state == Socket::AcceptingTCP)
				processTCPAccepting(slotId);
			else if (s.state == Socket::WorkingTCP)
			{
				processReceiving(slotId);
			}
			else if (s.state == Socket::WorkingUDP)
			{
				processReceiving(slotId);
			}
		}
		if (s.getfd() == InvalidSocket) {
			s.state = Socket::Empty;
		}
	}
	///////////////////////////////////////////////////////////////////////////
	// update all sockets
	void Peer::update(timeval &timeout)
	{
		//Lock _lock(*this);
#ifdef FROSTTOOLS_USE_POLL
		checkSlots_poll(timeout);
#else
		checkSlots_select(timeout);
#endif
		assert(lockedSocketsArray == false);

		lockedSocketsArray = true;
		//
		// 2. Process all sockets
		for (size_t i = 0; i < socketsAllocated; i++) {
			if(sockets[i].state != Socket::Empty)
				updateSlotState(i);
		}

		// 3. Process closing state ?
		if (listener) {
			for (size_t i = 0; i < socketsAllocated; i++)
				if (sockets[i].state == Socket::Dying)
					listener->onSlotClosed(this, i);
		}

		for (size_t i = 0; i < socketsAllocated; i++) {
			if (sockets[i].state == Socket::Dying)
				sockets[i].state = Socket::Empty;
		}

		lockedSocketsArray = false;
	}

	///////////////////////////////////////////////////////////////////////////////
	/// local call
	int Peer::checkSlotHandle(SlotHandle handle) const
	{
		if(handle >= socketsAllocated)
		{
			callError(InvalidSlot);
			return -1;
		}
		return 0;
	}

	void Peer::callError(NetErrorType error) const
	{
		writeLog(1, "Peer error: %s", getErrorString(error));
		/// TODO: proper event handling
	}

	const char * getErrorString(NetErrorType error)
	{
		switch(error)
		{
		case SocketCreation:
			return "Cannot create new socket";
		case ConnectionClosed:
			return "Connection was closed";
		case InvalidSlot:
			return "Invalid slot";
		case WouldBlock:
			return "Operation would block";
		case Memory:
			return "Memory allocation error";
		case BufferOverrun:
			return "Input buffer overrun";
		case SystemInterrupted:
			return "System interrupted";
		case InvalidArgument:
			return "Invalid argument";
		}
		return "Unknown error";
	}

	void Peer::writeLog(int level, const char * format, ...) const
	{
		va_list	ap;
		va_start(ap,format);
		const int MAX_LEN=4096;
		static char buffer[MAX_LEN];
	#ifdef _MSC_VER
			vsprintf_s(buffer, MAX_LEN,format,ap);
	#else
			vsnprintf(buffer, MAX_LEN,format,ap);
	#endif

		buffer[MAX_LEN-1]=0;
		printf("Peer says: %s\n", buffer);
		va_end(ap);
	}

	PeerAddress getLocalAddress()
	{
		PeerAddress result;
		char hostname[255];
		int len = gethostname(hostname, sizeof(hostname));
		hostent * he = gethostbyname(hostname);
		if(he != NULL)
		{
			result.setHostname(inet_ntoa(*(struct in_addr*)he->h_addr));
		}
		else
		{
			result.setHostname("127.0.0.1");
		}
		return result;
	}

	SocketSelector_Select::SocketSelector_Select()
	{
		FD_ZERO(&readfds);
		FD_ZERO(&writefds);
		FD_ZERO(&exceptfds);
	}
	/// add socket to selector
	void SocketSelector_Select::add(Socket * sock)
	{
		sockets.push_back(sock);
	}
	/// clear all sockets
	void SocketSelector_Select::clear()
	{
		FD_ZERO(&readfds);
		FD_ZERO(&writefds);
		FD_ZERO(&exceptfds);
		sockets.clear();
	}
	/// execute check for all stored sockets. Event data will be stored at each Socket::events field
	int SocketSelector_Select::check(timeval & timeout)
	{
		FD_ZERO(&readfds);
		FD_ZERO(&writefds);
		FD_ZERO(&exceptfds);
		int selectAwaits = 0;
		int selected = 0;
		int maxfds = 0;
		// 1. Fill socket sets
		for( size_t i = 0; i < sockets.size(); ++i )
		{
			Socket & s = *sockets[i];

			if(!s.validSocket())
				continue;

			int fd = s.getfd();

			if(fd > maxfds)
				maxfds = fd;

			if(s.shouldEventIn())
			{
				FD_SET(fd, &readfds);
			}

			if(s.shouldEventOut())
			{
				FD_SET(fd, &writefds);
			}

			if(s.shouldEventError())
			{
				FD_SET(fd, &exceptfds);
			}

			selectAwaits++;
		}

		if( selectAwaits > 0 )
		{
			do
			{
				selected = select(maxfds+1, &readfds, &writefds, &exceptfds, &timeout);
			}while(selected < 0 && Socket::getError() == SystemInterrupted );
		}

		/// gather events
		if(selected > 0)
		{
			for( size_t i = 0; i < sockets.size(); ++i )
			{
				Socket &s = *sockets[i];
				s.events = 0;
				int fd = s.getfd();
				if(FD_ISSET(fd, &readfds))
				{
					s.events |= Socket::EventIn;
				}
				if(FD_ISSET(fd, &writefds))
				{
					s.events |= Socket::EventOut;
				}
				if(FD_ISSET(fd, &exceptfds))
				{
					s.events |= Socket::EventError;
				}
			}
		}
		return selected;
	}
}
}
