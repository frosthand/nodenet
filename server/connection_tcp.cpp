//#include "stdafx.h"
#include <nodenet/transport_ip.h>

#include <stdlib.h>
#include <string.h>

#include <assert.h>
#include <stdarg.h>
#include <stdio.h>

#include <ctype.h>

namespace nodenet
{
	////////////////////////////////////////////////////////////////////////////////
	// TCP transport part
	////////////////////////////////////////////////////////////////////////////////
	Transport * ConnectionIP::getTransport()
	{
		return transport;
	}

	int ConnectionIP::sendSystemData(DataBufferPtr buffer, int requestId, int targetSlot, const EndPoint & address, size_t flags)
	{
		/// 1. Init header
		DataTicket ticket;
		ticket.requestId = requestId;
		ticket.targetSlot = targetSlot;
		ticket.buffer = buffer;
		ticket.transmitState = DataTicket::StateHeader;
		sendQueueSystem.pushTicket(ticket);
		return 0;
	}

	int ConnectionIP::sendUserData(DataBufferPtr buffer, int requestId, int targetSlot, const EndPoint & address, size_t flags)
	{
		/// 1. Init header
		DataTicket ticket;
		ticket.requestId = requestId;
		ticket.targetSlot = targetSlot;
		ticket.buffer = buffer;
		ticket.transmitState = DataTicket::StateHeader;
		sendQueueUser.pushTicket(ticket);
		return 0;
	}

	void ConnectionIP::dropPendingData(int * _packets, int * _bytes)
	{
		DataTicket ticket;
		int packets = 0;
		int bytes = 0;
		// empty system queue
		while(sendQueueSystem.popTicket(ticket))
		{
			packets++;
			bytes += ticket.buffer.size();
		}
		// empty user queue
		while(sendQueueUser.popTicket(ticket))
		{
			packets++;
			bytes += ticket.buffer.size();
		}

		if(_packets != NULL)
			*_packets = packets;
		if(_bytes != NULL)
			* _bytes = bytes;
	}

	void ConnectionIP::sendPendingData()
	{
		if(sendPendingData(this->sendQueueSystem, true) >= 0)
			sendPendingData(this->sendQueueUser, true);
	}

	int ConnectionIP::sendPendingData(TicketStorage & storage, bool sendAll)
	{
		int toSend = 0;
		// If we can send anything
		if(!socket.hasEvent(ip::Socket::EventOut))
		{
			transport->writeLog(1, "ConnectionIP::sendPendingData() - socket is locked for writing");
			return -1;
		}


		// Count total size
		/*
		guardSend.lock();
		for(std::list<DataTicket>::iterator it = sendQueue.begin(); it!=sendQueue.end();++it)
		{
			toSend += (sizeof(PacketHeaderTCP) + it->buffer.size());
		}
		this->transport->writeLog(1, "ConnectionIP::sendPendingData() - %d bytes to send in %d packets", toSend, sendQueue.size());
		guardSend.unlock();
		 */
		int result = 0;
		do
		{
			bool hasPendingData = false;

			if(sendingTicket.transmitState == DataTicket::StateEmpty)
			{
				if(!storage.popTicket(sendingTicket))
					break;

				sendingTicket.dataProcessed = 0;
				sendingTicket.transmitState = DataTicket::StateHeader;
			}

			PacketHeaderTCP header;

			void * data = NULL;
			size_t dataSize = 0;

			if(sendingTicket.transmitState == DataTicket::StateHeader)
			{
				header.requestId = sendingTicket.requestId;
				header.targetSlot = sendingTicket.targetSlot;
				header.totalSize = sendingTicket.buffer.size();

				data = (char*)&header + sendingTicket.dataProcessed;
				dataSize = sizeof(header) - sendingTicket.dataProcessed;
				transport->writeLog(2, "ConnectionIP::sendPendingData() - sending header of %d", dataSize);
			}
			else
			{
				data = sendingTicket.buffer.data() + sendingTicket.dataProcessed;
				dataSize = sendingTicket.buffer.size() - sendingTicket.dataProcessed;
				transport->writeLog(2, "ConnectionIP::sendPendingData() - sending data of %d", dataSize);
			}

			if(dataSize > 0)
			{
				int sent = socket.send(data, dataSize, NULL);

				if(sent > 0)
				{
					sendingTicket.dataProcessed += sent;
					// All data is sent
					if(sent == dataSize )
					{
						transport->writeLog(2, "ConnectionIP::sendPendingData() - sent %d bytes", dataSize);

						if(sendingTicket.transmitState == DataTicket::StateData && sendingTicket.remaining() == 0)
							sendingTicket.reset();
						else if(sendingTicket.transmitState == DataTicket::StateHeader && sendingTicket.dataProcessed == sizeof(PacketHeaderTCP))
						{
							sendingTicket.transmitState = DataTicket::StateData;
							sendingTicket.dataProcessed = 0;
						}
					}
					else
					{
						transport->writeLog(2, "ConnectionIP::sendPendingData() - partial sent %d of %d bytes", sent, dataSize);
						// Seems like not full packet is sent. Try it later
						break;
					}
				}
				else if(sent == 0)	// blocked completely
				{
					int packets = 0;
					int bytes = 0;
					dropPendingData(&packets, &bytes);
					this->transport->writeLog(1, "ConnectionIP::sendPendingData() - Dropped %d bytes in %d, %d bytes in active packet", bytes, packets, sendingTicket.buffer.size() );
					sendingTicket.transmitState = DataTicket::StateEmpty;
					result = -1;
				}
				else // sent < 0 - handle error
				{
					// TODO: handle errors here
					this->transport->writeLog(1, "ConnectionIP::sendPendingData() - Error during send");
					int packets = 0;
					int bytes = 0;
					dropPendingData(&packets, &bytes);
					this->transport->writeLog(1, "ConnectionIP::sendPendingData() - Dropped %d bytes in %d, %d bytes in active packet", bytes, packets, sendingTicket.buffer.size() );
					sendingTicket.transmitState = DataTicket::StateEmpty;
					result = -1;
				}
			}
		}while(sendingTicket.transmitState != DataTicket::StateEmpty);

		return result;
	}

	void ConnectionIP::close()
	{
		socket.close();
		this->transport->notifyConnectionClosed(this);
		transport->conUsers.remove(Ptr(this));
	}
}
