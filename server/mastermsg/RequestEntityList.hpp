/** THIS IS AN AUTOMATICALLY GENERATED FILE.  DO NOT MODIFY
 * BY HAND!!
 *
 * Generated by lcm-gen
 **/

#include <lcm/lcm_coretypes.h>

#ifndef __mastermsg_RequestEntityList_hpp__
#define __mastermsg_RequestEntityList_hpp__

#include <string>

namespace mastermsg
{

class RequestEntityList
{
    public:
        std::string node;
        std::string filter;

    public:
        inline int encode(void *buf, int offset, int maxlen) const;
        inline int getEncodedSize() const;
        inline int decode(const void *buf, int offset, int maxlen);
        inline static int64_t getHash();
        inline static const char* getTypeName();

        // LCM support functions. Users should not call these
        inline int _encodeNoHash(void *buf, int offset, int maxlen) const;
        inline int _getEncodedSizeNoHash() const;
        inline int _decodeNoHash(const void *buf, int offset, int maxlen);
        inline static int64_t _computeHash(const __lcm_hash_ptr *p);
};

int RequestEntityList::encode(void *buf, int offset, int maxlen) const
{
    int pos = 0, tlen;
    int64_t hash = getHash();

    tlen = __int64_t_encode_array(buf, offset + pos, maxlen - pos, &hash, 1);
    if(tlen < 0) return tlen; else pos += tlen;

    tlen = this->_encodeNoHash(buf, offset + pos, maxlen - pos);
    if (tlen < 0) return tlen; else pos += tlen;

    return pos;
}

int RequestEntityList::decode(const void *buf, int offset, int maxlen)
{
    int pos = 0, thislen;

    int64_t msg_hash;
    thislen = __int64_t_decode_array(buf, offset + pos, maxlen - pos, &msg_hash, 1);
    if (thislen < 0) return thislen; else pos += thislen;
    if (msg_hash != getHash()) return -1;

    thislen = this->_decodeNoHash(buf, offset + pos, maxlen - pos);
    if (thislen < 0) return thislen; else pos += thislen;

    return pos;
}

int RequestEntityList::getEncodedSize() const
{
    return 8 + _getEncodedSizeNoHash();
}

int64_t RequestEntityList::getHash()
{
    static int64_t hash = _computeHash(NULL);
    return hash;
}

const char* RequestEntityList::getTypeName()
{
    return "RequestEntityList";
}

int RequestEntityList::_encodeNoHash(void *buf, int offset, int maxlen) const
{
    int pos = 0, tlen;

    char* node_cstr = (char*) this->node.c_str();
    tlen = __string_encode_array(buf, offset + pos, maxlen - pos, &node_cstr, 1);
    if(tlen < 0) return tlen; else pos += tlen;

    char* filter_cstr = (char*) this->filter.c_str();
    tlen = __string_encode_array(buf, offset + pos, maxlen - pos, &filter_cstr, 1);
    if(tlen < 0) return tlen; else pos += tlen;

    return pos;
}

int RequestEntityList::_decodeNoHash(const void *buf, int offset, int maxlen)
{
    int pos = 0, tlen;

    int32_t __node_len__;
    tlen = __int32_t_decode_array(buf, offset + pos, maxlen - pos, &__node_len__, 1);
    if(tlen < 0) return tlen; else pos += tlen;
    if(__node_len__ > maxlen - pos) return -1;
    this->node.assign(((const char*)buf) + offset + pos, __node_len__ - 1);
    pos += __node_len__;

    int32_t __filter_len__;
    tlen = __int32_t_decode_array(buf, offset + pos, maxlen - pos, &__filter_len__, 1);
    if(tlen < 0) return tlen; else pos += tlen;
    if(__filter_len__ > maxlen - pos) return -1;
    this->filter.assign(((const char*)buf) + offset + pos, __filter_len__ - 1);
    pos += __filter_len__;

    return pos;
}

int RequestEntityList::_getEncodedSizeNoHash() const
{
    int enc_size = 0;
    enc_size += this->node.size() + 4 + 1;
    enc_size += this->filter.size() + 4 + 1;
    return enc_size;
}

int64_t RequestEntityList::_computeHash(const __lcm_hash_ptr *)
{
    int64_t hash = 0xdfdcc1f1d0916659LL;
    return (hash<<1) + ((hash>>63)&1);
}

}

#endif
