#include <winsock2.h>
#include "nodenet/transport_ip.h"

namespace nodenet {

const size_t DEFAULT_BUFLEN = 0xffff;

/// Initialized winsock2 library in win32
/// Does nothing in POSIX environment
class Network
{
protected:
	Network();
	~Network();
	static Network g_network;
};

Network Network::g_network;

Network::Network()
{
    //-----------------------------------------
    // Declare and initialize variables
    WSADATA wsaData;
    int iResult;

    unsigned long ulAddr = INADDR_NONE;
  
    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (iResult != 0) 
    {
        printf("WSAStartup failed: %d\n", iResult);
        
    }
}

Network::~Network()
{
	WSACleanup();
}

int Socket::close()
{
	int result = ::closesocket(sockfd);
	sockfd = INVALID_SOCKET;
	return result;
}

#ifdef _MSC_VER
/*
int Socket::getSysErrorCode()
{
	return WSAGetLastError();
}

const char * Socket::getSysErrorString(int errorcode)
{
	return strerror(errorcode);
}

const char * Socket::getError()
{
	return getSysErrorString(getSysErrorCode());
}*/

void ReportErrorWin32(int errorCode, const char *whichFunc)
{
	char errorMsg[500];       // Declare a buffer to hold the generated messages
	memset(errorMsg, sizeof(errorMsg),0);// Automatically NULL-terminate the strings

	switch(errorCode)
	{
		case 10014:
		sprintf(errorMsg, "Bad address.");            //
		break;
		case 10022:
		sprintf(errorMsg, "Invalid argument.");//
		break;
		case 10036:
		sprintf(errorMsg, "Operation now in progress.");//
		break;
		case 10037:
		sprintf(errorMsg, "Operation already in progress. ");//
		break;
		case 10038:
		sprintf(errorMsg, "Socket operation on non-socket.");//
		break;
		case 10040:
		sprintf(errorMsg, "Message too long.");//
		break;
		case 10048:
		sprintf(errorMsg, "Address already in use.");//
		break;
		case 10049:
		sprintf(errorMsg, "Cannot assign requested address.");//
		break;
		case 10050:
		sprintf(errorMsg, "Network is down.");//
		break;
		case 10053:
		sprintf(errorMsg, "Software caused connection abort.");//
		break;
		case 10054:
		sprintf(errorMsg, "Peer reset by remote side.");//
		break;
		case 10055:
		sprintf(errorMsg, "No buffer space available.");//
		break;
		case 10057:
		sprintf(errorMsg, "Socket is not connected.");//
		break;
		case 10060:
		sprintf(errorMsg, "Peer timed out.");//
		break;
		case 10061:
		sprintf(errorMsg, "Peer refused.");//
		break;
		case 10064:
		sprintf(errorMsg, "Host is down.");//
		break;
		/*  case :
		 sprintf(errorMsg, "");
		 break;
		 */default:
		//sprintf(caption, "Socket Indication");
		sprintf(errorMsg, "Call to %s returned error %d!", (char *)whichFunc, errorCode);
	}
	char caption[500];
	memset(caption, sizeof(caption),0);
	sprintf(caption, "Socket Error on function %s !", (char *)whichFunc);
	MessageBoxA(NULL, errorMsg, caption, MB_OK | MB_ICONERROR); // show it finally

//	fprintf(stderr, errorMsg);
	return;
};
#endif

NetErrorType Socket::getError()
{
	int err = WSAGetLastError();
	switch(err)
	{
	case WSAEWOULDBLOCK: return WouldBlock;
	}
	return Unknown;
}

int Socket::setBlocking(int value)
{
	assert(validSocket());
	u_long iMode = !value;
	if(ioctlsocket(sockfd,FIONBIO,&iMode)==SOCKET_ERROR)
		return -1;
	return 0;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}
